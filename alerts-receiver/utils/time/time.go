package time

import (
	"fmt"
	"time"
)

const defaultFormat string = "2006-01-02 15:04:05"

const hwAlertFormat string = "2006/01/02 15:04:05 MST:00"

func Str2time(formatTimeStr string) (formatTime time.Time, err error) {
	formatTime, err = time.Parse(defaultFormat, formatTimeStr)
	return formatTime, err
}

func Time2StampString(t time.Time) string {
	return fmt.Sprintf("%d", t.Unix())
}

func Str2StampString(timeString string, timeFormat string) (*string, error) {
	formatTime, err := time.Parse(timeFormat, timeString)
	if err != nil {
		return nil, err
	}
	str := fmt.Sprintf("%d", formatTime.Unix())
	return &str, nil
}

func HWTimeStr2StampString(timeString string) (*string, error) {
	return Str2StampString(timeString, hwAlertFormat)
}
