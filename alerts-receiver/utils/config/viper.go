package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func InitViper(configpath *string) {

	viper.SetConfigName("alerts-receiver")
	viper.AddConfigPath(*configpath)
	viper.AddConfigPath(".")    // 还可以在工作目录中查找配置
	err := viper.ReadInConfig() // 查找并读取配置文件
	if err != nil {             // 处理读取配置文件的错误
		panic(fmt.Errorf("fatal error config file: %s ", err))
	}
}
