package logger

import (
	"flag"
	"os"
	"strings"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	ownLogger        *zap.Logger
	ownSugaredLogger *zap.SugaredLogger
	infoLogPath      = flag.String("info.log.path", ".", "info log Path.")
	errLogPath       = flag.String("err.log.path", ".", "err log Path.")
	logLevel         = flag.String("log.level", "info", "debug or info")
	logType          = flag.String("log.type", "file", "stdout or file")
	logName          = flag.String("log.base.name", "alerts-receiver", "info log name default is 'alerts-receiver_info.log', err log name default is 'alerts-receiver_err.log'。")
)

func InitLogger() {
	// 设置一些基本日志格式 具体含义还比较好理解，直接看zap源码也不难懂
	encoder := zapcore.NewJSONEncoder(
		zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			FunctionKey:    zapcore.OmitKey,
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		})
	// 实现判断日志等级的interface

	infoLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.InfoLevel
	})
	errorLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})
	if *logLevel == "debug" {
		infoLevel = zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= zapcore.DebugLevel
		})
	}

	// 最后创建具体的Logger
	var core zapcore.Core
	if *logType == "file" {
		// 获取 info、error日志文件的io.Writer 抽象 getLogWriter() 在下方实现
		infoLog := strings.Join([]string{
			*infoLogPath,
			*logName + "_info.log",
		}, "/")
		infoWriter := getLogWriter(infoLog)
		errorLog := strings.Join([]string{
			*errLogPath,
			*logName + "_err.log",
		}, "/")
		errorWriter := getLogWriter(errorLog)
		core = zapcore.NewTee(
			zapcore.NewCore(encoder, zapcore.AddSync(infoWriter), infoLevel),
			zapcore.NewCore(encoder, zapcore.AddSync(errorWriter), errorLevel),
		)
	} else {
		core = zapcore.NewTee(
			zapcore.NewCore(encoder, zapcore.AddSync(os.Stdout), errorLevel),
			zapcore.NewCore(encoder, zapcore.AddSync(os.Stdout), infoLevel),
		)
	}

	//core := zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(os.Stdout, infoWriter, errorWriter), zapcore.DebugLevel)
	ownLogger = zap.New(core, zap.AddCaller(), zap.AddCallerSkip(1)) // 需要传入 zap.AddCaller() 才会显示打日志点的文件名和行数
	ownSugaredLogger = ownLogger.Sugar()
}

func getLogWriter(log string) zapcore.WriteSyncer {
	/*
		Filename: 日志文件的位置
		MaxSize：在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups：保留旧文件的最大个数
		MaxAges：保留旧文件的最大天数
		Compress：是否压缩/归档旧文件
	*/
	lumberJackLogger := &lumberjack.Logger{
		Filename:   log,
		MaxSize:    10,
		MaxBackups: 5,
		MaxAge:     30,
		LocalTime:  true,
		Compress:   true,
	}
	return zapcore.AddSync(lumberJackLogger)
}

func Debug(args ...interface{}) {
	ownSugaredLogger.Debug(args...)
}

func Debugf(template string, args ...interface{}) {
	ownSugaredLogger.Debugf(template, args...)
}

func Info(args ...interface{}) {
	ownSugaredLogger.Info(args...)
}

func Infof(template string, args ...interface{}) {
	ownSugaredLogger.Infof(template, args...)
}

func Warn(args ...interface{}) {
	ownSugaredLogger.Warn(args...)
}

func Warnf(template string, args ...interface{}) {
	ownSugaredLogger.Warnf(template, args...)
}

func Error(args ...interface{}) {
	ownSugaredLogger.Error(args...)
}

func Errorf(template string, args ...interface{}) {
	ownSugaredLogger.Errorf(template, args...)
}

func DPanic(args ...interface{}) {
	ownSugaredLogger.DPanic(args...)
}

func DPanicf(template string, args ...interface{}) {
	ownSugaredLogger.DPanicf(template, args...)
}

func Panic(args ...interface{}) {
	ownSugaredLogger.Panic(args...)
}

func Panicf(template string, args ...interface{}) {
	ownSugaredLogger.Panicf(template, args...)
}

func Fatal(args ...interface{}) {
	ownSugaredLogger.Fatal(args...)
}

func Fatalf(template string, args ...interface{}) {
	ownSugaredLogger.Fatalf(template, args...)
}
