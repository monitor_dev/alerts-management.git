package logger

func GetRegionsInfo(len int) {
	Infof("get regions len: %v", len)
}
func GetInstancesInfo(len int) {
	Infof("get instances len: %v", len)
}
func GetSlowlogsInfo(len int) {
	Infof("get slowlogs len: %v", len)
}
func GetSlowlogsLenInfo(len int, len1 int) {
	Infof("get slowlogs Context len: %v not least len: ", len, len1)
}
func SQLInfo(sql string) {
	Infof("start SQL: %v", sql)
}

func GetStartEndInfo(startts *string, endts *string) {
	ss := ""
	es := ""
	if startts != nil {
		ss = *startts
	}
	if endts != nil {
		es = *endts
	}
	Infof("get startts : %v ,endts : %v ", ss, es)
}
func GetSQLResultInfo(rows int64) {
	Infof("after exec sql,rows changed: %v ", rows)
}

// insertalerts
func DuplicateRecoverMessageInfo(labels []byte) {
	Infof("DuplicateRecoverMessag for labels: %v", string(labels))
}
func AlertStarttsChangedInfo(labels []byte) {
	Infof("AlertStarttsChanged for labels: %v", string(labels))
}

func AlertReceivedInfo(labels []byte, hash string) {
	Infof("AlertReceived for hash: %v labels: %v", hash, string(labels))
}
func AlertRecordInfo(alert []byte) {
	Infof(string(alert))
}

func CommonInfo(s string) {
	Infof(s)
}
