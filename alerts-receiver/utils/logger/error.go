package logger

// token
func TokenNilError(ip string) {
	Errorf("Token is nil client IP: %v", ip)
}
func TokenUnmarshalError(ip string) {
	Errorf("Unmarshal json  failed client IP: %v", ip)
}
func TokenCheckError(ip string) {
	Errorf("Token sign  check failed client IP: %v", ip)
}

// clickhouse
func ClickHouseConnectOpenError(err error) {
	Errorf("ClickHouseConnectOpenError by: %v", err)
}
func ClickHouseConnectPingExceptionError(code int32, message string, trace string) {
	Errorf("ClickHouseConnectPingExceptionError by: [%d] %s \n%s\n", code, message, trace)
}
func ClickHouseConnectPingError(err error) {
	Errorf("ClickHouseConnectPingError by: %v", err)
}

func GetRegionsError(err error) {
	Errorf("get regions failed by: %v", err.Error())
}
func GetInstancesError(err error) {
	Errorf("get instance failed by :%v", err.Error())
}
func GetSlowLogsError(err error) {
	Errorf("get slowlog failed by :%v", err.Error())
}
func RexMatchError(rex string, str string) {
	Errorf("rex:%v don't match string:%v", rex, str)
}
func OpenFileError(filename string, err error) {
	Errorf("failed open file: %v by: %v", filename, err)
}
func ExecSQLError(sql string, err error) {
	Errorf("%v exec failed by: %v", sql, err)
}
func GetSQLResultError(sql string, err error) {
	Errorf("%v get result failed by: %v", sql, err)
}

// redis
func SetRedisError(sql string, err error) {
	Errorf("%v redis set failed by: %v", sql, err)
}
func GetRedisError(sql string, err error) {
	Errorf("%v redis get failed by: %v", sql, err)
}
func Redis2StructError(sql string, err error) {
	Errorf("%v redis to struct failed by: %v", sql, err)
}
func NilPointertError(pointer string) {
	Errorf("pointer: %v is nil ", pointer)
}

// kafka
func Struct2MessageError(err error) {
	Errorf("Struct2MessageError by: %v", err)
}
func WriteMessageError(err error) {
	Errorf("WriteMessageError by: %v", err)
}

// http
func HttpPostError(url string, err error) {
	Errorf("HttpPostError to url: %v by: %v", url, err)
}
func HttpReadResponseError(err error) {
	Errorf("HttpReadResponseError  by: %v", err)
}

// json
func JsonMarshalError(st interface{}, err error) {
	Errorf("JsonMarshalError of struct : %v by: %v", st, err)
}
