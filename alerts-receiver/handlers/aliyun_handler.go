package handlers

import (
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/common"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/controller"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/response"
	"github.com/kataras/iris/v12"
)

func HandleAliyunAlert(ctx iris.Context) {
	id := ctx.Params().GetString("id")
	if len(id) == 0 {
		ctx.JSON(response.NewResponse(response.DataEmptyErr.Code, nil, response.DataEmptyErr.Msg))
		return
	}
	at := new(common.ProjectInfo)
	at.SetType("aliyun")
	at.SetId(id)
	ea := common.AliyunAlert{}
	ctx.ReadJSON(&ea)
	err := controller.HandleParticularAlert(&ea, at)
	if err != nil {
		ctx.JSON(response.NewResponse(response.SystemErr.Code, nil, response.SystemErr.Msg))
		return
	}
	ctx.JSON(response.NewResponse(response.NoErr.Code, nil, response.NoErr.Msg))
}
