package common

import (
	"strconv"
	"strings"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/time"
)

type AppAlert interface {
	ToCommonAlert() (*CommonAlert, error)
}

// 项目相关
func (p *ProjectInfo) SetType(s string) {
	p.ProjectType = s
}

func (p *ProjectInfo) GetType() string {
	return p.ProjectType
}

func (p *ProjectInfo) SetId(s string) {
	p.ProjectId = s
}

func (p *ProjectInfo) GetId() string {
	return p.ProjectId
}

// Hash 相关
func (h *HashInfo) setAlertId(s string) {
	h.alertId = s
}

func (h *HashInfo) setObjectId(s string) {
	h.objectId = s
}

func (h *HashInfo) setGlobalId(s string) {
	h.globalId = s
}

func (h *HashInfo) GetAlertId() string {
	return h.alertId
}

func (h *HashInfo) GetObjectId() string {
	return h.objectId
}

func (h *HashInfo) GetGlobalId() string {
	return h.globalId
}

// 根据redis缓存,更新告警状态
func (ca *CommonAlert) LoadCache(h HandleInfo) {
	// 无缓存 当已关闭处理
	if h.HandleStatus == "已关闭" || h.HandleStatus == "" {
		ca.HandleStatus = "新告警"

	} else if h.HandleStatus == "新告警" || h.HandleStatus == "未确认" {
		// 可恢复告警恢复时，可跳过确认，直接变为待关闭
		if ca.Recoverable == "true" && ca.AlertStatus == "resovled" {
			ca.HandleStatus = "待关闭"
		} else {
			ca.HandleStatus = "未确认"
		}
	} else if h.HandleStatus == "处理中" {
		// 可恢复告警恢复时，可跳过确认，直接变为待关闭
		if ca.Recoverable == "true" && ca.AlertStatus == "resovled" {
			ca.HandleStatus = "待关闭"
		} else {
			ca.HandleStatus = "处理中"
		}
	}
}

func (ca *CommonAlert) DumpCache(h *HandleInfo) {
	h.AlertCache = ca.AlertCache
	h.AlertNocache = ca.AlertNocache
}

func (ca *CommonAlert) Hash() {
	ca.setAlertId(HashObject(ca.AlertData))
	ca.setObjectId(HashObject(ca.AlertData))
	ca.setGlobalId(Join(ca.ProjectType, "-", ca.ProjectId, "-", ca.Severity, "-", ca.objectId, "-", ca.alertId))
}

// 仅进行格式转换
func (e *EventAlert) ToCommonAlert(pi *ProjectInfo) *CommonAlert {
	c := new(CommonAlert)
	c.ProjectInfo = *pi
	c.AlertInfo = e.AlertInfo
	return c
}

func (e *AlertmanagerAlert) ToCommonAlert(pi *ProjectInfo) *CommonAlert {
	c := new(CommonAlert)
	c.ProjectInfo = *pi
	c.StartTime = time.Time2StampString(e.StartsAt)
	c.Recoverable = "true"
	c.AlertStatus = string(e.Status())
	c.SrcData = map[string]interface{}{"labels": e.Labels, "Annotations": e.Annotations}
	// labels := map[string]string{}
	// json.Unmarshal(json.Marshal(e.Labels),&labels)
	// annotations := map[string]string{}
	// json.Unmarshal(json.Marshal(e.Labels),&annotations)
	if c.AlertStatus == "resolved" {
		c.EndTime = time.Time2StampString(e.EndsAt)
	}
	return c
}

func (e *UcloudAlert) Status() string {
	if e.RecoveryTime == 0 {
		return "firing"
	}
	return "resolved"
}

func (e *UcloudAlert) ToCommonAlert(pi *ProjectInfo) *CommonAlert {
	c := new(CommonAlert)
	c.ProjectInfo = *pi
	c.StartTime = strconv.Itoa(int(e.AlarmTime))
	c.AlertStatus = e.Status()
	c.Recoverable = "true"
	if c.AlertStatus == "resolved" {
		c.EndTime = strconv.Itoa(int(e.RecoveryTime))
	}
	c.ObjectData = map[string]interface{}{"Region": e.Region, "ResourceId": e.ResourceId, "ResourceType": e.ResourceType}
	c.AlertData = map[string]interface{}{"AlertName": e.MetricName}
	for metricname, resourceids := range ucloudConfig {
		if metricname == e.MetricName {
			for _, resurceid := range resourceids {
				// 禁止测试环境使用disaster组
				if resurceid == e.ResourceId {
					c.NotifyData = map[string]interface{}{"flybook": map[string]interface{}{"groups": []string{"disaster"}, "emails": []string{"huanghaohe@52tt.com"}}}
					break
				}
			}
			break
		}
	}

	c.AlertText = e.Content
	return c
}
func (e *AliyunAlert) Status() string {
	/*OK：正常。
	ALERT：报警。
	INSUFFICIENT_DATA：无数据。
	*/
	if e.AlertState == "OK" {
		return "resolved"
	} else { // e.AlertState == "INSUFFICIENT_DATA"  e.AlertState == "ALERT"
		return "firing"
	}
}
func (e *AliyunAlert) Severity() string {
	if e.AlertState == "CRITICAL" {
		return "critical"
	} else if e.AlertState == "WARN" {
		return "warning"
	} else {
		return "info"
	}
}

func (e *AliyunAlert) ToCommonAlert(pi *ProjectInfo) *CommonAlert {
	c := new(CommonAlert)
	c.ProjectInfo = *pi
	status := e.Status()
	if status == "firing" {
		c.StartTime = e.Timestamp
	} else if status == "resolved" {
		c.EndTime = e.Timestamp
	}
	c.Recoverable = "true"
	c.AlertStatus = status
	c.ObjectData = map[string]interface{}{"Dimensions": e.Dimensions, "InstanceName": e.InstanceName, "MetricProject": e.MetricProject}
	if e.AlertState == "INSUFFICIENT_DATA" {
		c.AlertData = map[string]interface{}{"alertname": e.AlertName + " 无法取值"}
	} else {
		c.AlertData = map[string]interface{}{"alertname": e.AlertName}
	}
	c.Severity = e.Severity()
	return c
}

func (h *HuaweiyunMessage) Status() string {
	e := h.Message
	if e.Template_variable.IsAlarm {
		return "firing"
	} else {
		return "resolved"
	}
}

func (h *HuaweiyunMessage) Severity() string {
	e := h.Message
	if e.Template_variable.AlarmLevel == "紧急" {
		return "disaster"
	} else if e.Template_variable.AlarmLevel == "重要" {
		return "critical"
	} else if e.Template_variable.AlarmLevel == "次要" {
		return "warning"
	} else {
		// "提示"
		return "info"
	}
}

func (h *HuaweiyunMessage) ToCommonAlert(pi *ProjectInfo) *CommonAlert {
	msgString := h.MessageString
	msg := h.Message
	json.UnmarshalFromString(msgString, &msg)
	h.Message = msg

	e := h.Message
	c := new(CommonAlert)
	c.ProjectInfo = *pi
	c.AlertStatus = h.Status()
	c.Recoverable = "true"
	ts := strconv.Itoa(e.Time / 1000)
	if c.AlertStatus == "firing" {
		c.StartTime = ts
	} else {
		c.EndTime = ts
	}
	// namespace 资源大类 dimensionname 资源子类
	c.ObjectData = map[string]interface{}{"Namespace": e.Template_variable.Namespace, "DimensionName": e.Template_variable.DimensionName,
		"Region": e.Template_variable.Region, "ResourceName": e.Template_variable.ResourceName, "ResourceId": e.Template_variable.ResourceId}
	if strings.HasPrefix(h.Subject, "[MRS]") {
		c.AlertData = map[string]interface{}{"alertname": h.Subject}
	} else {
		c.AlertData = map[string]interface{}{"alertname": e.Template_variable.MetricName}
	}

	if strings.HasPrefix(h.Subject, "[MRS]") {
		c.Severity = "critical"
		c.AlertInfo.AlertText = h.MessageString

	} else {
		c.AlertInfo.AlertText = e.Sms_content
		c.Severity = h.Severity()
	}
	if h.Topic_urn == "urn:smn:cn-north-4:08b242bf6b80f5d52f98c00a6a6139d2:DBA" && ((c.Severity == "disaster" && c.AlertStatus == "firing") || strings.HasPrefix(h.Subject, "[MRS]")) {
		c.NotifyData = map[string]interface{}{"flybook": map[string]interface{}{"groups": []string{"dba-huaweiyun"}, "emails": []string{"linan@52tt.com", "zhouhancang@52tt.com"}}}
	} else {
		return nil
	}
	bt, _ := json.Marshal(h)
	//bt, _ := json.Marshal(h)
	jsonData := map[string]interface{}{}
	_ = json.Unmarshal(bt, jsonData)
	c.SrcData = jsonData
	c.SrcData["Subject"] = h.Subject
	c.SrcData["Topic_urn"] = h.Topic_urn
	return c
}
