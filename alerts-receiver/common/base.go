package common

import (
	"bytes"
	"crypto/md5"
	"fmt"

	jsoniter "github.com/json-iterator/go"
)

// 该配置已实现key排序
var json = jsoniter.ConfigCompatibleWithStandardLibrary

func ToJson(obj interface{}) ([]byte, error) {
	bs, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return bs, nil
}

func Md5SumByte(Byte []byte) (Hash string) {
	Sum := md5.Sum(Byte)
	return fmt.Sprintf("%x", Sum)
}

func HashObject(obj interface{}) string {
	var a string
	if obj != nil {
		abyte, err := ToJson(obj)
		if err != nil {
			a = ""
		} else {
			a = Md5SumByte(abyte)
		}
	} else {
		a = ""
	}
	return a
}

func Join(strings ...string) string {
	var buffer bytes.Buffer
	for _, s := range strings {
		buffer.WriteString(s)
	}
	return buffer.String()
}
