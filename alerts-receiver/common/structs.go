package common

import (
	"time"

	"github.com/prometheus/common/model"
)

// 项目信息
type ProjectInfo struct {
	ProjectType string //告警接入类型 Alertmanager,Ucloud,Aliyun,Huaweiyun,Grafana,Callback,Event(自定义事件)
	ProjectId   string //项目ID 用于识别同一告警类型下的不同通道，比如两个不同的prometheus集群，当需要认证时，该值作为认证ID
}

// 对象与告警唯一id
type HashInfo struct {
	alertId  string //告警唯一ID
	objectId string //对象唯一ID
	globalId string //全局唯一ID alerttype-projectid-severity-alertId-objectId
}

// redis状态缓存
type HandleInfo struct {
	AlertNocache
	AlertCache
}

type AlertCache struct {
	StartTime   string `json:"StartTime"`   // "1627639497" 秒级时间戳,表示故障开始时间 当AlertStatus为problem时生效 //当无该字段时，或故障未恢复且时间改变时，沿用老时间
	Recoverable string `json:"Recoverable"` // true/false 是否发送恢复通知，若调用方在故障恢复时不调用此接口，则配置为false ,避免因未接收到恢复而导致告警升级
	AlertStatus string `json:"AlertStatus"` // firing/resovled 告警状态，当Recoverable为true时生效，标记当前告警为故障或恢复
}

type AlertNocache struct {
	HandleStatus string `json:"HandleStatus"` // 新告警 （初次告警） 未确认 （确认） 处理中 （恢复） 待关闭 与mongodb状态对应
}

// 告警信息
type AlertInfo struct {
	AlertCache
	ObjectData map[string]interface{} `json:"ObjectData"` // 告警对象唯一标识,同一项目下唯一，用于告警抑制，分组，通知策略配置或告警模板定制 例如 {"hostname":"aliyun-blackbox-01","ip":"10.59.63.4","supplier":"ucloud"}
	AlertData  map[string]interface{} `json:"AlertData"`  //  告警事件唯一标识,同一项目下唯一，用于告警抑制，分组，通知策略配置或告警模板定制 例如 {"alertname":"CPU使用率达到90%"}
	NotifyData map[string]interface{} `json:"NotifyData"` // 告警通知字段 当前支持飞书（群组key为groups,值需要服务端提供，个人使用emails或mobiles，飞书关联联系方式） 格式为{"flybook"：{"groups":["groupname"],"mobiles":["13917104943"],"emails":["liaopeng@52tt.com"]}}
	Severity   string                 `json:"Severity"`   //告警等级 "disaster/critical/warning"
	EndTime    string                 `json:"EndTime"`    // "1627639497" 秒级时间戳，表示故障恢复时间 当AlertStatus为ok时生效
	AlertText  string                 `json:"AlertText"`  // 告警文本 "HBase_rcmd_core1(10.10.113.160:59100) 内存可用率是6.917% ，  可用内存是 1.615GiB"
	SrcData    map[string]interface{} `json:"SrcData"`    // 原始告警数据 例如 {"alertname":"AggregatedAPIDown","cluster":"prod-bj2-02","env":"production","name":"v1beta1.metrics.k8s.io","namespace":"default","prometheus":"monitoring/prometheus-quicksilver","severity":"warning"}
}

// 通用告警结构体
type CommonAlert struct {
	ProjectInfo
	AlertInfo
	HashInfo
	AlertNocache
}

// 自定义事件告警结构体，结构与通用结构体一致，避免再次转换
type EventAlert struct {
	AlertInfo
}

// alertmanager告警结构体
type AlertmanagerAlert struct {
	model.Alert
	// The authoritative timestamp.
	UpdatedAt time.Time
	Timeout   bool
}

type UcloudAlert struct {
	SessionID    string `json:"SessionID"`
	Region       string `json:"Region"`
	ResourceType string `json:"ResourceType"`
	ResourceId   string `json:"ResourceId"`
	MetricName   string `json:"MetricName"`
	AlarmTime    int64  `json:"AlarmTime"`
	Value        int    `json:"value"`
	ValueUnit    string `json:"ValueUnit"`
	RecoveryTime int64  `json:"RecoveryTime"`
	Content      string `json:"content"`
}

type AliyunAlert struct {
	AlertName       string `json:"alertName"`
	AlertState      string `json:"alertState"`
	CurValue        string `json:"curValue"`
	Dimensions      string `json:"dimensions"`
	Expression      string `json:"expression"`
	InstanceName    string `json:"instanceName"`
	MetricName      string `json:"metricName"` // cluster.cpu.allocatable
	MetricProject   string `json:"metricProject"`
	Namespace       string `json:"namespace"` // kafka
	PreTriggerLevel string `json:"preTriggerLevel"`
	RuleId          string `json:"ruleId"`
	Timestamp       string `json:"timestamp"`
	TriggerLevel    string `json:"triggerLevel"`
	UserId          string `json:"userId"`
}

/*


"message": "{"message_type":"alarm","alarm_id":"al1628855618260kLQxaJpvZ","alarm_name":"alarm-uu-disk","alarm_status":"ok","time":1630782913740,
"namespace":"AGT.ECS","metric_name":"load_average5","dimension":"instance_id:b3dd4d43-c5e9-4352-a41c-81ef39cf2bd3","period":1,"filter":"Raw data",
"comparison_operator":"\u003e","value":1,"unit":"","count":3,"alarmValue":[{"time":1630782900000,"value":0.97},{"time":1630782840000,"value":1.12},
{"time":1630782780000,"value":1.08}],"sms_content":"[华北-北京四][企业项目：声洞][重要告警恢复]尊敬的xieshangcheng：主机监控-云服务器 “hw-bj-uu-script-01” （私网IP：10.215.0.58，公网IP：124.70.85.7，ID：b3dd4d43-c5e9-4352-a41c-81ef39cf2bd3）的（Agent）5分钟平均负载当前数据：0.97，于2021/09/05 03:15:13 GMT+08:00恢复正常，详情请访问云监控服务。",

"template_variable":{"AccountName":"xieshangcheng","Namespace":"主机监控","DimensionName":"云服务器","ResourceName":"hw-bj-uu-script-01",
"MetricName":"（Agent）5分钟平均负载","IsAlarm":false,"IsCycleTrigger":false,"AlarmLevel":"重要","Region":"华北-北京四","ResourceId":"b3dd4d43-c5e9-4352-a41c-81ef39cf2bd3",
"PrivateIp":"10.215.0.58","PublicIp":"124.70.85.7","AlarmRule":"","CurrentData":"0.97","AlarmTime":"2021/09/05 03:15:13 GMT+08:00",
"DataPoint":{"2021/09/05 03:13:00 GMT+08:00":"1.08","2021/09/05 03:14:00 GMT+08:00":"1.12","2021/09/05 03:15:00 GMT+08:00":"0.97"},
"DataPointTime":["2021/09/05 03:15:00 GMT+08:00","2021/09/05 03:14:00 GMT+08:00","2021/09/05 03:13:00 GMT+08:00"],"AlarmRuleName":"alarm-uu-disk",
"AlarmId":"al1628855618260kLQxaJpvZ","AlarmDesc":"","MonitoringRange":"资源分组","IsOriginalValue":true,"Period":"","Filter":"原始值","ComparisonOperator":"\u003e","Value":"1.00",
"Unit":"","Count":3,"EventContent":"","Link":"https://console.huaweicloud.com/ces/?region=cn-north-4#/alarms/detail?alarmId=al1628855618260kLQxaJpvZ","EpName":"声洞","IsIEC":false,
"IsAgentEvent":false}
}"

*/

type HuaweiyunTemplateVar struct {
	Namespace          string            `json:"Namespace"`
	DimensionName      string            `json:"DimensionName"`
	ResourceName       string            `json:"ResourceName"`
	MetricName         string            `json:"MetricName"`
	IsAlarm            bool              `json:"IsAlarm"`
	IsCycleTrigger     bool              `json:"IsCycleTrigger"`
	AlarmLevel         string            `json:"AlarmLevel"`
	Region             string            `json:"Region"`
	ResourceId         string            `json:"ResourceId"`
	PrivateIp          string            `json:"PrivateIp"`
	PublicIp           string            `json:"PublicIp"`
	AlarmRule          string            `json:"AlarmRule"`
	CurrentData        string            `json:"CurrentData"`
	AlarmTime          string            `json:"AlarmTime"`
	DataPoint          map[string]string `json:"DataPoint"`     // {\"2021/09/05 03:10:00 GMT+08:00\":\"91.41%\",\"2021/09/05 03:15:00 GMT+08:00\":\"91.32%\",\"2021/09/05 03:20:00 GMT+08:00\":\"91.46%\"}
	DataPointTime      []string          `json:"DataPointTime"` // [\"2021/09/05 03:20:00 GMT+08:00\",\"2021/09/05 03:15:00 GMT+08:00\",\"2021/09/05 03:10:00 GMT+08:00\"]
	AlarmRuleName      string            `json:"AlarmRuleName"`
	AlarmId            string            `json:"AlarmId"`
	AlarmDesc          string            `json:"AlarmDesc"`
	MonitoringRange    string            `json:"MonitoringRange"`
	IsOriginalValue    bool              `json:"IsOriginalValue"`
	Period             string            `json:"Period"`
	Filter             string            `json:"Filter"`
	ComparisonOperator string            `json:"ComparisonOperator"`
	Value              string            `json:"Value"`
	Unit               string            `json:"Unit"`
	EventContent       string            `json:"EventContent"`
	Count              int               `json:"Count"`
	Link               string            `json:"Link"`
	EpName             string            `json:"EpName"`
	IsIEC              bool              `json:"IsIEC"`
	IsAgentEvent       bool              `json:"IsAgentEvent"`
}

type HuaweiAlarmValue struct {
	Time  int     `json:"time"`
	Value float32 `json:"value"`
}
type HuaweiyunAlert struct {
	Message_type        string `json:"message_type"`
	Alarm_id            string `json:"alarm_id"`
	Alarm_name          string `json:"alarm_name"`
	Alarm_status        string `json:"alarm_status"`
	Time                int    `json:"time"`
	Namespace           string `json:"namespace"`
	Metric_name         string `json:"metric_name"`
	Dimension           string `json:"dimension"`
	Period              int    `json:"period"`
	Filter              string `json:"filter"`
	Comparison_operator string `json:"comparison_operator"`

	Value int    `json:"value"`
	Unit  string `json:"unit"`
	Count int    `json:"count"`

	AlarmValue        []HuaweiAlarmValue   `json:"alarmValue"`
	Sms_content       string               `json:"sms_content"`
	Template_variable HuaweiyunTemplateVar `json:"template_variable"`
}

type HuaweiyunMessage struct {
	Signature         string         `json:"signature"`
	Subject           string         `json:"subject"`
	Topic_urn         string         `json:"topic_urn"`
	Message_id        string         `json:"message_id"`
	Signature_version string         `json:"signature_version"`
	Type              string         `json:"type"`
	Message           HuaweiyunAlert `json:"exchanged_message"`
	MessageString     string         `json:"message"`
	// Unsubscribe_url   string         `json:"unsubscribe_url"`  避免误点击，取消该解析
	Signing_cert_url string `json:"signing_cert_url"`
	Timestamp        string `json:"timestamp"`
}

type ParticularAlert interface {
	ToCommonAlert(*ProjectInfo) *CommonAlert
}
