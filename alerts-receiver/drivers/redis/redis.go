package redis

import (
	"fmt"
	"strings"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/logger"
	"github.com/gomodule/redigo/redis"
	"github.com/spf13/viper"
)

type RedisSQL struct {
	client redis.Conn
}

func (r *RedisSQL) SetStruct(Hash string, Struct interface{}) error {
	_, err := r.client.Do("hmset", redis.Args{}.Add(Hash).AddFlat(Struct)...)
	if err != nil {
		logger.SetRedisError(fmt.Sprintf("%v %v", Hash, Struct), err)
		return err
	}
	return nil
}
func (r *RedisSQL) GetStruct(Hash string, Struct interface{}) error {
	v, err := redis.Values(r.client.Do("hgetall", Hash))
	if err != nil {
		logger.GetRedisError(fmt.Sprintf("%v %v", Hash, Struct), err)
		return err
	}
	if err = redis.ScanStruct(v, Struct); err != nil {

		logger.GetRedisError(fmt.Sprintf("%v %v", Hash, Struct), err)
		return err
	}
	return nil
}

func (r *RedisSQL) Close() {
	r.client.Close()
}

var pool *redis.Pool

func InitRedisPool() {
	url := viper.GetString(strings.Join([]string{"redis", "url"}, "."))
	password := viper.GetString(strings.Join([]string{"redis", "password"}, "."))
	db := viper.GetInt(strings.Join([]string{"redis", "db"}, "."))
	pool = &redis.Pool{
		MaxIdle:   100,  /*最大的空闲连接数*/
		MaxActive: 1000, /*最大的激活连接数*/
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", url, redis.DialPassword(password), redis.DialDatabase(db))
			//c, err := redis.Dial("tcp", "192.168.198.29:6379", redis.DialPassword(""))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}
func NewRedisSQL() *RedisSQL {
	return &RedisSQL{client: pool.Get()}
}
