package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/logger"
)

func Post(url string, data interface{}) ([]byte, error) {

	// 超时时间：5秒
	client := &http.Client{Timeout: 5 * time.Second}
	jsonStr, err := json.Marshal(data)
	logger.CommonInfo(string(jsonStr))
	if err != nil {
		logger.JsonMarshalError(data, err)
		return nil, err
	}
	resp, err := client.Post(url, defaultContentType, bytes.NewBuffer(jsonStr))
	if err != nil {
		logger.HttpPostError(url, err)
		return nil, err
	}
	defer resp.Body.Close()
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.HttpReadResponseError(err)
		return nil, err
	}
	return result, nil
}
