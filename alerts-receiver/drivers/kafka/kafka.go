package kafka

import (
	"context"
	"encoding/json"
	"errors"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/logger"
	"github.com/segmentio/kafka-go"
)

var KafkaWriter *kafka.Writer
var KafkaReader *kafka.Reader

// make a writer that produces to topic-A, using the least-bytes distribution
func InitKafkaWriter() {
	KafkaWriter = &kafka.Writer{
		Addr:     kafka.TCP("192.168.22.154:9092"),
		Topic:    "alerts-stream",
		Balancer: &kafka.LeastBytes{},
		// 异步
		//Async: true,
	}

	/*

		if err := w.Close(); err != nil {
			log.Fatal("failed to close writer:", err)
		}
	*/
}

func Struct2Message(key string, st interface{}) (*kafka.Message, error) {
	bt, err := json.Marshal(st)
	if err != nil {
		return nil, err
	}
	return &kafka.Message{Key: []byte(key),
		Value: bt}, nil
}

func WriteMessages(ctx context.Context, msgs ...kafka.Message) error {
	switch errs := KafkaWriter.WriteMessages(ctx, msgs...).(type) {
	case nil:
		return nil
	case kafka.WriteErrors:
		for i := range msgs {
			if errs[i] != nil {
				logger.WriteMessageError(errs[i])
			}
		}
		return errors.New("many error to write message")
	default:
		return nil
	}
}

func InitKafkaReader() {
	KafkaReader = kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{"192.168.22.154:9092"},
		GroupID:  "consumer-group-id",
		Topic:    "alerts-stream",
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	/*

		if err := w.Close(); err != nil {
			log.Fatal("failed to close writer:", err)
		}
	*/
}

// func ReadMessages(ctx context.Context, msgs ...kafka.Message) error {
// 	for {
// 		m, err := KafkaReader.ReadMessage(context.Background())
// 		if err != nil {
// 			break
// 		}
// 		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
// 	}

// 	if err := r.Close(); err != nil {
// 		log.Fatal("failed to close reader:", err)
// 	}
// }
