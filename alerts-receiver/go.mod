module gitee.com/monitor_dev/alerts-management/alerts-receiver

go 1.16

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/gomodule/redigo v1.7.1-0.20190724094224-574c33c3df38
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/kataras/iris/v12 v12.1.8
	github.com/kr/text v0.2.0 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/prometheus/common v0.30.0
	github.com/segmentio/kafka-go v0.4.17
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/spf13/viper v1.8.1
	github.com/valyala/fasthttp v1.28.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
