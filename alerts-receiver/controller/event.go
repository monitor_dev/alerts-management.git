package controller

import (
	"github.com/spf13/viper"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/common"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/drivers/http"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/drivers/redis"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/logger"
)

var notifyerURL string

func InitURL() {
	notifyerURL = viper.GetString("notifyer.url")
	if notifyerURL == "" {
		panic("no notifyer.url in config found")
	}
}
func HandleParticularAlert(ea common.ParticularAlert, at *common.ProjectInfo) error {
	ca := ea.ToCommonAlert(at)
	if ca == nil {
		return nil
	}
	ca.Hash()

	rds := redis.NewRedisSQL()
	defer rds.Close()
	var hi common.HandleInfo
	err := rds.GetStruct(ca.GetGlobalId(), &hi)
	if err != nil {
		logger.GetRedisError(ca.GetGlobalId(), err)
		return err
	}
	ca.LoadCache(hi)
	ca.DumpCache(&hi)
	err = rds.SetStruct(ca.GetGlobalId(), &hi)
	if err != nil {
		logger.SetRedisError(ca.GetGlobalId(), err)
		return err
	}
	res, err := http.Post(notifyerURL, ca)
	if err != nil {
		logger.HttpPostError(notifyerURL, err)
	}
	logger.CommonInfo(string(res))
	return nil

	/*
		msg, err := kafka.Struct2Message(ca.GetType(), ca)
		if err != nil {
			logger.Struct2MessageError(err)
			return err
		}

		err = kafka.WriteMessages(context.Background(), *msg)
		if err != nil {
			logger.WriteMessageError(err)
			return err
		}
		return nil
	*/
}
