package main

import (
	"flag"
	"fmt"
	"os"

	"gitee.com/monitor_dev/alerts-management/alerts-receiver/controller"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/drivers/kafka"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/drivers/redis"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/handlers"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/response"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/config"
	"gitee.com/monitor_dev/alerts-management/alerts-receiver/utils/logger"
	"github.com/kataras/iris/v12"
	"github.com/spf13/viper"
)

var (
	// Set during go build
	version   string
	gitCommit string
	buildTime string
	// 命令行参数

	confPath = flag.String("config.path", "./", "Config Path.")
)

func main() {

	versionFlag := flag.Bool("version", false, "print the version")
	flag.Parse()
	// 版本打印
	if *versionFlag {
		fmt.Printf("Version: %s\n", version)
		fmt.Printf("Commit: %s\n", gitCommit)
		fmt.Printf("BuildTime: %s\n", buildTime)
		os.Exit(0)
	}
	// 初始化配置文件
	config.InitViper(confPath)
	logger.InitLogger()
	redis.InitRedisPool()
	kafka.InitKafkaWriter()
	controller.InitURL()
	app := iris.New()
	//app.UseGlobal(handlers.InsertAlertsBefore)
	app.Post("/api/v1/event/{id:string}", handlers.HandleEvent)
	app.Post("/api/v1/alertmanager/{id:string}", handlers.HandleEvent)
	app.Post("/api/v1/ucloud/{id:string}", handlers.HandleUcloudAlert)
	app.Post("/api/v1/aliyun/{id:string}", handlers.HandleEvent)
	app.Post("/api/v1/huaweiyun/{id:string}", handlers.HandleHuaweiyunAlert)
	//app.DoneGlobal(handler)
	app.Get("/-/healthy", func(ctx iris.Context) {
		ctx.JSON(response.NewResponse(response.NoErr.Code, nil, response.NoErr.Msg))
	})
	app.Run(iris.Addr(viper.GetString("server.listen")))

}
