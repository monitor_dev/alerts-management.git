# `邮局`告警系统
对告警最后一公里的问题解决系统。

[![post](https://img.shields.io/badge/告警系统-邮局-da282a)](https://gitee.com/mickeybee/alert_post)


### 子功能：
- 快递员：对飞书群组机器人的支持
- 邮差：对高级机器人的支持

### 入口：
- 支持[Grafana](https://grafana.com/docs/grafana/latest/alerting/notifications/)的`webhook`
- 支持[Alertmanager](https://prometheus.io/docs/alerting/latest/configuration/#webhook)的`webhook`

