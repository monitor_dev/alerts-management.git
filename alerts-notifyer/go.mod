module gitee.com/monitor_dev/alerts-management/alerts-notifyer

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/go-delve/delve v1.7.0 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/prometheus/alertmanager v0.22.2
	github.com/prometheus/client_golang v1.11.0
	github.com/spf13/viper v1.8.1
	go.uber.org/zap v1.18.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
