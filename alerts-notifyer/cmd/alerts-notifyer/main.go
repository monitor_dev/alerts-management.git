package main

import (
	"flag"
	"fmt"
	"os"

	_ "net/http/pprof"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/controller"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/metrics"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/user_bot"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	// gs "github.com/swaggo/gin-swagger"
	// "github.com/swaggo/gin-swagger/swaggerFiles"
)

var (
	// Set during go build
	version   string
	gitCommit string
	buildTime string
	// 命令行参数
	confPath = flag.String("config.path", "./", "Config Path.")
	confName = flag.String("config.name", "simple_alert", "default name 'simple_alert'")
	// 命令行参数
	listenAddr = flag.String("web.listen-port", ":58060", "An port to listen on for web interface and telemetry.")
)

func InitViper() {
	viper.SetConfigName(*confName)
	viper.AddConfigPath(*confPath)
	viper.AddConfigPath(".")    // 还可以在工作目录中查找配置
	err := viper.ReadInConfig() // 查找并读取配置文件
	if err != nil {             // 处理读取配置文件的错误
		panic(fmt.Errorf("fatal error config file: %s ", err))
	}
}

// @title 告警快递
// @version 0.1.0
// @description 自定义机器人转发告警信息

// @contact.name 蓝宝石的傻话
// @contact.url https://gitee.com/mickeybee
// @contact.email mickey_zzc@126.com

// @license.name LGPL v2.1
// @license.url https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
func main() {
	versionFlag := flag.Bool("version", false, "print the version")
	flag.Parse()
	// 版本打印
	if *versionFlag {
		fmt.Printf("Version: %s\n", version)
		fmt.Printf("Commit: %s\n", gitCommit)
		fmt.Printf("BuildTime: %s\n", buildTime)
		os.Exit(0)
	}

	InitViper()
	logger.InitLogger()
	user_bot.InitFeishuBot()
	dropbox.InitTemplate()
	dropbox.InitCommonAlertTemplate()
	dropbox.InitKeysMap()
	router := gin.New()
	//router := gin.Default()
	router.Use(logger.GinLogger(), logger.GinRecovery(true))
	//router.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler))

	router.GET("/metrics", gin.WrapH(metrics.PrometheusColletcor()))

	router.GET("/-/healthy", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "healthy ok.",
		})
	})

	alert_v1 := router.Group("alert/v1")
	{
		//alert_v1.POST("/", )
		alert_v1.POST("/comm", controller.CommAlertToFeishuCustombot)
		alert_v1.POST("/alertmanger", controller.AlertToFeishuCustombot)
		alert_v1.POST("/alertmanager", controller.AlertToFeishuCustombot)
		alert_v1.POST("/grafana", controller.GrafanaToFeishuCustombot)
	}
	router.POST("api/v1/commonalerts", controller.CommonAlertToFeishuCustombot)
	router.Run(*listenAddr)
}
