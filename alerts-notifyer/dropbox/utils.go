package dropbox

import (
	"encoding/json"
	"io/ioutil"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/viper"
)

var levelMap map[string]string
var typeMap map[string]string
var exceptMap map[string][]string

func init() {
	levelMap = map[string]string{"disaster": "灾难", "critical": "严重", "warning": "警告"}
	typeMap = map[string]string{"host": "主机", "kafka_topic_consumer": "kafka", "application": "应用"}
	exceptMap = map[string][]string{"common": {"alertname", "idc_name", "level", "severity", "cmdb_level", "cmdb_developer", "cmdb_maintainer", "cmdb_type", "flybook_groups", "flybook_emails", "flybook_mobiles"},
		"host":                 {"cmdb_addr"},
		"kafka_topic_consumer": {"cmdb_name"},
		"application":          {"cmdb_name"}}
}

var keysMap map[string]string

func InitKeysMap() {
	kmp := viper.GetString("keysmap.path")
	if kmp == "" {
		panic("keysmap.path in config not found")
	}
	kpb, err := ioutil.ReadFile(kmp)
	if err != nil {
		panic("keysmap file read err")
	}
	json.Unmarshal(kpb, &keysMap)
}

func Time2Str(alertTime time.Time) string {
	cstZone, _ := time.LoadLocation("Asia/Shanghai")
	return alertTime.In(cstZone).Format("2006-01-02 15:04:05")
}

func GetNow() string {
	return Time2Str(time.Now())
}

func StampStr2Str(ss string) string {
	number, err := strconv.Atoi(ss)
	if err != nil {
		return ""
	}
	alertTime := time.Unix(int64(number), 0)
	return Time2Str(alertTime)
}

func GetLevel(level string) string {
	level = strings.ToLower(level)
	v, ok := levelMap[level]
	if ok {
		return v
	} else {
		return level
	}
}
func GetType(cmdbtype string) string {
	cmdbtype = strings.ToLower(cmdbtype)
	v, ok := typeMap[cmdbtype]
	if ok {
		return v
	} else {
		return cmdbtype
	}
}

func CopyMap(oldMap map[string]string) (newMap map[string]string) {
	newMap = map[string]string{}
	for k, v := range oldMap {
		newMap[k] = v
	}
	return newMap
}

func ExceptLabel(labelMap map[string]string, cmdbType string) map[string]string {
	newLabelMap := CopyMap(labelMap)
	for _, commonLabel := range exceptMap["common"] {
		delete(newLabelMap, commonLabel)

	}
	for _, typeLabel := range exceptMap[cmdbType] {
		delete(newLabelMap, typeLabel)
	}
	return newLabelMap
}

func KeysMap(key string) string {
	v, ok := keysMap[key]
	if !ok {
		return key
	}
	return v
}
