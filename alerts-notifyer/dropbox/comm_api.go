package dropbox

import (
	"fmt"
	"time"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/auth"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"github.com/prometheus/alertmanager/template"
	"github.com/spf13/viper"
)

/*
'{
	"title": "CPU告警",     // 可选，标题信息
	"msg":"CPU异常告警。",   // 必选，通知信息
	"labels":{             // 必选，标签信息
			"cmdb_type" : "host",     // 可选，api的内部标签协议动态匹配
			"cmdb_name": "yw_prometheus"
	},
	"url": ""              // 可选，待定
}'
*/

/*
// KV is a set of key/value string pairs.
type KV map[string]string

// Remove returns a copy of the key/value set without the given keys.
func (kv KV) Remove(keys []string) KV {
	keySet := make(map[string]struct{}, len(keys))
	for _, k := range keys {
		keySet[k] = struct{}{}
	}

	res := KV{}
	for k, v := range kv {
		if _, ok := keySet[k]; !ok {
			res[k] = v
		}
	}
	return res
}

func (kv KV) Add(keys map[string]string) KV {
	res := KV{}
	for k, v := range kv {
		res[k] = v
	}
	for k, v := range keys {
		res[k] = v
	}
	return res
}

*/
type AlertPostCommApi struct {
	Title  string             `json:"title,omitempty"`
	Msg    string             `json:"msg"`
	StarTs string             `json:"startsTs,omitempty"`
	EndTs  string             `json:"endTs,omitempty"`
	Labels template.KV        `json:"labels,omitempty"`
	Values map[string]float64 `json:"values,omitempty"`
}

func (api *AlertPostCommApi) ReportCC() {
	url := viper.GetString("report.api")
	if len(url) == 0 {
		return
	}
	// 读取url上的飞书字段
	logger.Debug("接收报告的接口是：", url)

	report := Report{}
	report.AlertPostCommApi = *api

	// 从配置支持open api
	access_key := viper.GetString("report.access_key")
	secret := viper.GetString("report.secret")

	if len(access_key) == 0 || len(secret) == 0 {
		resMsg, _ := report.SendReportMsg(url)
		logger.Infof("无鉴权调用的请求返回是:%v", resMsg)
		return
	}
	report.Access_key = access_key
	report.Random = auth.RandStringBytesMaskImprSrc(8)
	report.Ts = fmt.Sprint(time.Now().UnixNano() / 1e6)

	jsonStr := json.ToJsonIgnoreError(report)
	token := auth.TokenCreate(jsonStr, secret)
	resMsg, _ := report.SendReportMsgByToken(url, token)
	logger.Infof("有鉴权调用的请求返回是:%v", resMsg)
}

// 项目信息
type ProjectInfo struct {
	ProjectType string //告警接入类型 Alertmanager,Ucloud,Aliyun,Huaweiyun,Grafana,Callback,Event(自定义事件)
	ProjectId   string //项目ID 用于识别同一告警类型下的不同通道，比如两个不同的prometheus集群，当需要认证时，该值作为认证ID
}

// 对象与告警唯一id
type HashInfo struct {
	AlertId  string //告警唯一ID
	ObjectId string //对象唯一ID
	GlobalId string //全局唯一ID alerttype-projectid-severity-alertId-objectId
}

// redis状态缓存
type HandleInfo struct {
	AlertNocache
	AlertCache
}

type AlertCache struct {
	StartTime   string `json:"StartTime"`   // "1627639497" 秒级时间戳,表示故障开始时间 当AlertStatus为problem时生效 //当无该字段时，或故障未恢复且时间改变时，沿用老时间
	Recoverable string `json:"Recoverable"` // true/false 是否发送恢复通知，若调用方在故障恢复时不调用此接口，则配置为false ,避免因未接收到恢复而导致告警升级
	AlertStatus string `json:"AlertStatus"` // firing/resovled 告警状态，当Recoverable为true时生效，标记当前告警为故障或恢复
}

type AlertNocache struct {
	HandleStatus string `json:"HandleStatus"` // 新告警 （初次告警） 未确认 （确认） 处理中 （恢复） 待关闭 与mongodb状态对应
}

// 告警信息
type AlertInfo struct {
	AlertCache
	ObjectData map[string]interface{} `json:"ObjectData"` // 告警对象唯一标识,同一项目下唯一，用于告警抑制，分组，通知策略配置或告警模板定制 例如 {"hostname":"aliyun-blackbox-01","ip":"10.59.63.4","supplier":"ucloud"}
	AlertData  map[string]interface{} `json:"AlertData"`  //  告警事件唯一标识,同一项目下唯一，用于告警抑制，分组，通知策略配置或告警模板定制 例如 {"alertname":"CPU使用率达到90%"}
	NotifyData map[string]interface{} `json:"NotifyData"` // 告警通知字段 当前支持飞书（群组key为groups,值需要服务端提供，个人使用emails或mobiles，飞书关联联系方式） 格式为{"flybook"：{"groups":["groupname"],"mobiles":["13917104943"],"emails":["liaopeng@52tt.com"]}}
	Severity   string                 `json:"Severity"`   //告警等级 "disaster/critical/warning"
	EndTime    string                 `json:"EndTime"`    // "1627639497" 秒级时间戳，表示故障恢复时间 当AlertStatus为ok时生效
	AlertText  string                 `json:"AlertText"`  // 告警文本 "HBase_rcmd_core1(10.10.113.160:59100) 内存可用率是6.917% ，  可用内存是 1.615GiB"
	SrcData    map[string]interface{} `json:"SrcData"`    // 原始告警数据 例如 {"alertname":"AggregatedAPIDown","cluster":"prod-bj2-02","env":"production","name":"v1beta1.metrics.k8s.io","namespace":"default","prometheus":"monitoring/prometheus-quicksilver","severity":"warning"}
}

func (ai AlertInfo) GetFlybookGroups() []interface{} {
	nd := ai.NotifyData
	fb, ok := nd["flybook"].(map[string]interface{})
	if !ok {
		logger.Info("flybook tag in commonalert  is  all nil ,sent msg failed")
		return nil
	}
	groups, ok := fb["groups"].([]interface{})
	if !ok {
		logger.Info("flybook groups  not  in commonalert   ,sent msg failed")
		return nil
	}
	if len(groups) == 0 {
		logger.Info("len flybook groups in commonalert is  all nil ,sent msg failed")
		return nil
	}
	return groups
}

// 通用告警结构体
type CommonAlert struct {
	ProjectInfo
	AlertInfo
	HashInfo
	AlertNocache
}

// 自定义事件告警结构体，结构与通用结构体一致，避免再次转换
type EventAlert struct {
	ProjectInfo
	AlertInfo
}
