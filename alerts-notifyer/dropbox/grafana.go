package dropbox

import (
	"fmt"
	"strings"

	alertTemplate "github.com/prometheus/alertmanager/template"
)

/*
{
  "dashboardId":1,
  "evalMatches":[
    {
      "value":1,
      "metric":"Count",
      "tags":{}
    },
    {
      "value": 76,
      "metric": "TT_Backend(user-blacklist-redis)",
      "tags": {
        "resource_name": "user-blacklist-redis",
        "tag": "TT_Backend"
      }
    }
  ],
  "imageUrl":"https://grafana.com/static/assets/img/blog/mixed_styles.png",
  "message":"Notification Message",
  "orgId":1,
  "panelId":2,
  "ruleId":1,
  "ruleName":"Panel Title alert",
  "ruleUrl":"http://localhost:3000/d/hZ7BuVbWz/test-dashboard?fullscreen\u0026edit\u0026tab=alert\u0026panelId=2\u0026orgId=1",
  "state":"alerting",
  "tags":{
    "tag name":"tag value"
  },
  "title":"[Alerting] Panel Title alert"
}
*/
const (
	grafanaTitle = "Grafana告警通知"
	grafanaMsg   = "Grafana告警通知内容: "
)

type GrafanaWebhook struct {
	Title       string              `json:"title"`
	State       string              `json:"state"`
	Msg         string              `json:"message"`
	EvalMatches []*GrafanaEvalMatch `json:"evalMatches"`
}

type GrafanaEvalMatch struct {
	Tags   map[string]string `json:"tags,omitempty"`
	Metric string            `json:"metric"`
	Value  float64           `json:"value"`
}

func (alert *GrafanaWebhook) GrafanaWebhookToAlertPostCommApi(zone string) []*AlertPostCommApi {
	resComm := []*AlertPostCommApi{}
	title := grafanaTitle
	if alert.Title != "" {
		title = alert.Title
	}
	if zone != "" {
		title = "【信息来自节点" + zone + "】" + title
	}
	for _, eval := range alert.EvalMatches {
		res := AlertPostCommApi{}
		res.Title = title
		res.Labels = alertTemplate.KV(eval.Tags)
		msg := grafanaMsg
		if alert.Msg != "Notification Message" || alert.Msg != "" {
			msg = alert.Msg
		}
		value := strings.Join([]string{
			"Value值是:",
			fmt.Sprintf("%f", eval.Value),
		}, "")
		res.Values = map[string]float64{
			eval.Metric: eval.Value,
		}
		res.Msg = strings.Join([]string{
			msg,
			eval.Metric,
			value,
		}, "，")
		resComm = append(resComm, &res)
	}

	return resComm
}
