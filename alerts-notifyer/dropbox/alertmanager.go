package dropbox

import (
	"bytes"
	"text/template"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	alertTemplate "github.com/prometheus/alertmanager/template"
	"github.com/spf13/viper"
)

/*
{
  "receiver": "api",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "监控客户端节点断线测试",
        "hostname": "office",
        "idc": "office",
        "instance": "192.168.9.228:9100",
        "job": "node",
        "monitor_node": "office",
				"monitor_type": "node",
				"cmdb_type": "host",
				"cmdb_ip": "10.10.13.24",
        "severity": "critical"
      },
      "annotations": {
        "description": "office(192.168.9.228:9100)监控客户端离线已超出 1 分钟，使用10.10.13.24来查cmdb."
      },
      "startsAt": "2020-03-31T13:42:01.832416449+08:00",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://192.168.9.228:3000/graph?g0.expr=up%7Bjob%21~%22windows%22%7D+%3D%3D+0\u0026g0.tab=1",
      "fingerprint": "75d239f6807c5d07"
    },
    {
      "status": "resolved",
      "labels": {
        "alertname": "监控客户端节点断线",
        "hostname": "office-230",
        "idc": "office",
        "instance": "192.168.9.230:9100",
        "job": "node",
        "monitor_node": "office",
        "monitor_type": "node",
        "severity": "critical"
      },
      "annotations": {
        "description": "office-230(192.168.9.230:9100)监控客户端离线已超出 1 分钟."
      },
      "startsAt": "2020-03-31T13:53:31.832416449+08:00",
      "endsAt": "2020-03-31T13:55:01.832416449+08:00",
      "generatorURL": "http://192.168.9.228:3000/graph?g0.expr=up%7Bjob%21~%22windows%22%7D+%3D%3D+0\u0026g0.tab=1",
      "fingerprint": "cc2f000ccaa0a92c"
    }
  ],
  "groupLabels": {
    "alertname": "监控客户端节点断线"
  },
  "commonLabels": {
    "alertname": "监控客户端节点断线",
    "idc": "office",
    "job": "pi-node",
    "monitor_node": "office",
    "monitor_type": "node",
    "severity": "critical"
  },
  "commonAnnotations": {},
  "externalURL": "http://p-01:9093",
  "version": "4",
  "groupKey": "{}:{alertname=\"监控客户端节点断线\"}"
}
*/

type PromeAlerts alertTemplate.Data

var tpl *template.Template

func InitTemplate() {
	path := viper.GetString("template.path")
	if path == "" {
		panic("config template.path not found")
	}
	var err error
	tpl, err = template.New("tt_alert.tmpl").Option("missingkey=zero").Funcs(template.FuncMap{"GetNow": GetNow, "Time2Str": Time2Str, "GetLevel": GetLevel, "GetType": GetType, "ExceptLabel": ExceptLabel}).ParseFiles(path)
	//tpl,err = template.ParseFiles(path)
	if err != nil {
		panic(err)
	}
	//tpl.Option("missingkey=zero").Funcs(template.FuncMap{"GetNow": GetNow,"Time2Str":Time2Str,"GetLevel":GetLevel,"GetType":GetType,"ExceptLabel":ExceptLabel})
}

func PromeAlertToAlertPostCommApi(alert alertTemplate.Alert, zone string) AlertPostCommApi {
	logger.Debugf("接收到的告警信息：%s", alert)
	res := AlertPostCommApi{}
	res.Title = "Prometheus告警通知"
	res.Labels = alertTemplate.KV(alert.Labels)
	name, found := alert.Labels["alertname"]
	if found {
		res.Title = name
		//res.Labels.Remove([]string{"alertname"})
	}

	/*
	   然后,您可以使用buff.String()获取字符串结果,或使用buff.Bytes()获得[]字节结果.
	*/

	//alertTemplate :=``

	//.Parse(alertTemplate)

	var buff bytes.Buffer
	if err := tpl.Execute(&buff, alert); err != nil {
		panic(err)
	}
	res.StarTs = Time2Str(alert.StartsAt)
	if alert.EndsAt.After(alert.StartsAt) {
		res.EndTs = Time2Str(alert.EndsAt)
	}
	res.Msg = buff.String()
	/*
		res.Msg = "Prometheus告警通知信息。"
		msg, found := alert.Annotations["description"]

		if found {
			res.Msg = strings.Join(
				[]string{timeInfo, msg},
				"\n",
			)
		} else {
			res.Msg = timeInfo
			for k, v := range alert.Annotations {
				res.Msg = strings.Join(
					[]string{res.Msg, k + ":" + v},
					"\n",
				)
			}
		}
	*/

	return res
}

func (p *PromeAlerts) Firing(zone string) []AlertPostCommApi {

	promAlerts := p.Alerts.Firing()
	if nil == promAlerts {
		return nil
	}
	res := []AlertPostCommApi{}
	for _, alert := range promAlerts {
		commApi := PromeAlertToAlertPostCommApi(alert, zone)
		res = append(res, commApi)
	}
	return res
}

func (p *PromeAlerts) Resolved(zone string) []AlertPostCommApi {
	promAlerts := p.Alerts.Resolved()
	if nil == promAlerts {
		return nil
	}
	res := []AlertPostCommApi{}
	for _, alert := range promAlerts {
		commApi := PromeAlertToAlertPostCommApi(alert, zone)
		res = append(res, commApi)
	}
	return res
}
