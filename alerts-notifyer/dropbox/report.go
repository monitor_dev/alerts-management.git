package dropbox

import (
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
)

/*
{
  "title": string,
  "msg": string,
  "startsTs": string,
  "endTs": string,
  "labels": {
      "key01": string,
      "key02": string
  },
  "values": {
      "value01": float64,
      "value02": float64
  }
}
*/

type Report struct {
	AlertPostCommApi
	Ts         string `json:"ts,omitempty"`
	Access_key string `json:"access_key,omitempty"`
	Random     string `json:"random,omitempty"`
}

func (report *Report) SendReportMsg(url string) (string, error) {
	//   -H 'Content-Type: application/json' \
	headerJson := &http.HeaderOption{
		Name:  "Content-Type",
		Value: "application/json",
	}
	reportJson, err := json.ToJson(report)
	if nil != err {
		logger.Fatal("decode err")
	}

	return http.PostRequest(url, reportJson, *headerJson)
}

func (report *Report) SendReportMsgByToken(url, token string) (string, error) {
	//   -H 'Content-Type: application/json' \
	headerJson := &http.HeaderOption{
		Name:  "Content-Type",
		Value: "application/json",
	}
	tokenHead := &http.HeaderOption{
		Name:  "Open-Api-Auth",
		Value: token,
	}
	reportJson, err := json.ToJson(report)
	if nil != err {
		logger.Fatal("decode err")
	}

	logger.Debugf("Post body: %v", reportJson)
	return http.PostRequest(url, reportJson, *headerJson, *tokenHead)
}
