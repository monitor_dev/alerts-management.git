package dropbox

import (
	"bytes"
	"text/template"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"github.com/spf13/viper"
)

/*
{
  "receiver": "api",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "监控客户端节点断线测试",
        "hostname": "office",
        "idc": "office",
        "instance": "192.168.9.228:9100",
        "job": "node",
        "monitor_node": "office",
				"monitor_type": "node",
				"cmdb_type": "host",
				"cmdb_ip": "10.10.13.24",
        "severity": "critical"
      },
      "annotations": {
        "description": "office(192.168.9.228:9100)监控客户端离线已超出 1 分钟，使用10.10.13.24来查cmdb."
      },
      "startsAt": "2020-03-31T13:42:01.832416449+08:00",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://192.168.9.228:3000/graph?g0.expr=up%7Bjob%21~%22windows%22%7D+%3D%3D+0\u0026g0.tab=1",
      "fingerprint": "75d239f6807c5d07"
    },
    {
      "status": "resolved",
      "labels": {
        "alertname": "监控客户端节点断线",
        "hostname": "office-230",
        "idc": "office",
        "instance": "192.168.9.230:9100",
        "job": "node",
        "monitor_node": "office",
        "monitor_type": "node",
        "severity": "critical"
      },
      "annotations": {
        "description": "office-230(192.168.9.230:9100)监控客户端离线已超出 1 分钟."
      },
      "startsAt": "2020-03-31T13:53:31.832416449+08:00",
      "endsAt": "2020-03-31T13:55:01.832416449+08:00",
      "generatorURL": "http://192.168.9.228:3000/graph?g0.expr=up%7Bjob%21~%22windows%22%7D+%3D%3D+0\u0026g0.tab=1",
      "fingerprint": "cc2f000ccaa0a92c"
    }
  ],
  "groupLabels": {
    "alertname": "监控客户端节点断线"
  },
  "commonLabels": {
    "alertname": "监控客户端节点断线",
    "idc": "office",
    "job": "pi-node",
    "monitor_node": "office",
    "monitor_type": "node",
    "severity": "critical"
  },
  "commonAnnotations": {},
  "externalURL": "http://p-01:9093",
  "version": "4",
  "groupKey": "{}:{alertname=\"监控客户端节点断线\"}"
}
*/

var commonAlertTpl, huaweiyunAlertTpl, ucloudAlertTpl *template.Template

func InitCommonAlertTemplate() {
	path := viper.GetString("template.commonalertpath")
	if path == "" {
		panic("config template.path not found")
	}
	var err error
	commonAlertTpl, err = template.New("common_alert.tmpl").Option("missingkey=zero").Funcs(template.FuncMap{"KeysMap": KeysMap, "GetNow": GetNow, "StampStr2Str": StampStr2Str, "GetLevel": GetLevel, "GetType": GetType, "ExceptLabel": ExceptLabel}).ParseFiles(path)
	//tpl,err = template.ParseFiles(path)
	if err != nil {
		panic(err)
	}
	// 新增华为云模板
	path = viper.GetString("template.huaweiyunalertpath")
	if path == "" {
		panic("config template.path not found")
	}
	huaweiyunAlertTpl, err = template.New("huaweiyun_alert.tmpl").Option("missingkey=zero").Funcs(template.FuncMap{"KeysMap": KeysMap, "GetNow": GetNow, "StampStr2Str": StampStr2Str, "GetLevel": GetLevel, "GetType": GetType, "ExceptLabel": ExceptLabel}).ParseFiles(path)
	//tpl,err = template.ParseFiles(path)
	if err != nil {
		panic(err)
	}
	// 新增ucloud模板
	path = viper.GetString("template.ucloudalertpath")
	if path == "" {
		panic("config template.path not found")
	}
	ucloudAlertTpl, err = template.New("ucloud_alert.tmpl").Option("missingkey=zero").Funcs(template.FuncMap{"KeysMap": KeysMap, "GetNow": GetNow, "StampStr2Str": StampStr2Str, "GetLevel": GetLevel, "GetType": GetType, "ExceptLabel": ExceptLabel}).ParseFiles(path)
	//tpl,err = template.ParseFiles(path)
	if err != nil {
		panic(err)
	}

	//tpl.Option("missingkey=zero").Funcs(template.FuncMap{"GetNow": GetNow,"Time2Str":Time2Str,"GetLevel":GetLevel,"GetType":GetType,"ExceptLabel":ExceptLabel})
}

func CommAlertToFeishuPostTemplateV1(alertComm *CommonAlert) *string {
	var buff bytes.Buffer
	if alertComm.ProjectType == "huaweiyun" {
		if err := huaweiyunAlertTpl.Execute(&buff, alertComm); err != nil {
			logger.Error("template effcet error", err)
		}
	} else if alertComm.ProjectType == "ucloud" {
		if err := ucloudAlertTpl.Execute(&buff, alertComm); err != nil {
			logger.Error("template effcet error", err)
		}
	} else {
		if err := commonAlertTpl.Execute(&buff, alertComm); err != nil {
			logger.Error("template effcet error", err)
		}
	}
	/*
		res := &FeishuCustomBotComm{
			MsgType: "post",

			Content: buff.String(),
		}
	*/
	str := buff.String()
	return &str
}

/*
func CommonAlertToAlertPostCommApi(alert CommonAlert) AlertPostCommApi {
	logger.Debugf("接收到的告警信息：%s", alert)
	res := AlertPostCommApi{}
	res.Title = "告警标题"
	res.Labels = alertTemplate.KV(map[string]string{})
	name, found := alert.AlertData["alertname"].(string)
	if found {
		res.Title = name
	}
	var buff bytes.Buffer
	if err := commonAlertTpl.Execute(&buff, alert); err != nil {
		panic(err)
	}

	if alert.AlertStatus == "resolved" {
		number_end, err := strconv.Atoi(alert.EndTime)
		if err != nil {
			return res
		}
		res.StarTs = Time2Str(time.Unix(int64(number_end), 0))
	}
	res.Msg = buff.String()

	return res
}
*/
