package controller

import (
	"net/http"
	"plugin"
	"sync"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/custom_bot"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/user_bot"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/prometheus/alertmanager/template"
	"github.com/spf13/viper"
)

// AlertToFeishuCustombot Alertmanger转发到飞书的自定义机器人
// @Summary  Alertmanger转发到飞书的自定义机器人
// @Description 支持转发到飞书群的自定义机器人,使用富文本信息
// @Tags Alertmanager转发用
// @Accept application/json
// @Produce application/json
// @Param feishu query string false "对应飞书配置的key"
// @Success 200 {object} _ResponsePostList
// @Router /alert/v1/alertmanager [post]
func AlertToFeishuCustombot(c *gin.Context) {
	logger.Info("请求接口是：", c.Request.URL.Path)

	// 读取url上的飞书字段
	feishuTag := c.Query("feishu")
	logger.Info("请求feishuTag是：", feishuTag)
	/*
		if feishuTag == "" {
			logger.Error("url缺少必要参数'feishu'")
			c.JSON(http.StatusBadRequest, gin.H{"error": "url缺少必要参数'feishu'。"})
			return
		}
	*/
	zone := c.Query("zone")
	if zone == "" {
		zone = viper.GetString("global.zone")
	}

	// 从配置匹配相关的飞书机器人
	// 获取方式修改为labels:flybook_groups,适应多告警组场景
	//url := viper.GetString(strings.Join([]string{"feishu.custombot", feishuTag, "url"}, "."))
	//secret := viper.GetString(strings.Join([]string{"feishu.custombot", feishuTag, "secret"}, "."))
	var alertJson dropbox.PromeAlerts

	if err := c.ShouldBindBodyWith(&alertJson, binding.JSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		logger.Debugf("data: %v", binding.Form)
		return
	}
	logger.Info("收到告警labels：", alertJson)
	// 加载cmdb插件,将资源对象关联至联系人
	pluginEnable := viper.GetBool("plugin.enable")
	if pluginEnable {
		var failedFlag = false
		pluginPath := viper.GetString("plugin.path")
		pluginFuncName := viper.GetString("plugin.function")
		pluginUrl := viper.GetString("plugin.url")
		//加载so文件
		p, err := plugin.Open(pluginPath)
		if err != nil {
			failedFlag = true
			logger.Error("plugin .so file open failed ", pluginPath, err)
		}
		//搜索关键字
		var pluginFunc plugin.Symbol
		if !failedFlag {
			pluginFunc, err = p.Lookup(pluginFuncName)
			if err != nil {
				failedFlag = true
				logger.Error("plugin lookup function  failed", pluginFuncName, err)
			}
		}
		//关键字断言
		//relabelAlerts = []template.Alert
		if !failedFlag {
			var waitGroup sync.WaitGroup
			for _, alert := range alertJson.Alerts {
				waitGroup.Add(1)
				go func(wg *sync.WaitGroup, alert template.Alert) {
					_, err = pluginFunc.(func(*template.Alert, string) (*template.Alert, error))(&alert, pluginUrl)
					if err != nil {
						logger.Error("call cmdb to get userid  failed: by ", err)
					}
					wg.Done()
				}(&waitGroup, alert)
			}
			waitGroup.Wait()
		}
	}
	//任何一条告警发送错误，都不应该影响其他告警
	var errors []error
	var waitGroup sync.WaitGroup
	for _, alertFiring := range alertJson.Firing(zone) {
		waitGroup.Add(1)
		go func(wg *sync.WaitGroup, alertFiring dropbox.AlertPostCommApi) {
			logger.Info("即将发送告警labels：", alertFiring.Labels)
			err := custom_bot.FeishuSendCommMsg(&alertFiring, feishuTag)
			if err != nil {
				errors = append(errors, err)
			}
			err = user_bot.FeishuSendCommMsg(&alertFiring)
			if err != nil {
				errors = append(errors, err)
			}
			wg.Done()
		}(&waitGroup, alertFiring)
	}
	for _, alertResolved := range alertJson.Resolved(zone) {
		waitGroup.Add(1)
		go func(wg *sync.WaitGroup, alertResolved dropbox.AlertPostCommApi) {
			logger.Info("即将发送恢复labels：", alertResolved.Labels)
			err := custom_bot.FeishuSendCommMsg(&alertResolved, feishuTag)
			if err != nil {
				errors = append(errors, err)
			}
			err = user_bot.FeishuSendCommMsg(&alertResolved)
			if err != nil {
				errors = append(errors, err)
			}
			wg.Done()
		}(&waitGroup, alertResolved)
	}
	c.JSON(200, gin.H{
		"message": "handle ok.", "errors": errors,
	})
}
