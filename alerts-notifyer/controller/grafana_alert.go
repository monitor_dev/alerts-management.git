package controller

import (
	"net/http"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/custom_bot"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// GrafanaToFeishuCustombot Grafana转发到飞书的自定义机器人
// @Summary  Grafana转发到飞书的自定义机器人
// @Description 支持转发到飞书群的自定义机器人,使用富文本信息
// @Tags Grafana转发用
// @Accept application/json
// @Produce application/json
// @Param feishu query string false "对应飞书配置的key"
// @Success 200 {object} _ResponsePostList
// @Router /alert/v1/grafana [post]
func GrafanaToFeishuCustombot(c *gin.Context) {
	logger.Info("请求接口是：", c.Request.URL.Path)
	// 读取url上的飞书字段
	feishuTag := c.Query("feishu")
	/*
		if feishuTag == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "url缺少必要参数'feishu'。"})
			return
		}
	*/
	zone := c.Query("zone")
	if zone == "" {
		zone = viper.GetString("global.zone")
	}
	/*
		// 从配置匹配相关的飞书机器人
		url := viper.GetString(strings.Join([]string{"feishu.custombot", feishuTag, "url"}, "."))
		secret := viper.GetString(strings.Join([]string{"feishu.custombot", feishuTag, "secret"}, "."))
	*/
	var alertJson dropbox.GrafanaWebhook
	if err := c.ShouldBindJSON(&alertJson); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	for _, alert := range alertJson.GrafanaWebhookToAlertPostCommApi(zone) {
		err := custom_bot.FeishuSendCommMsg(alert, feishuTag)
		if err != nil {
			c.JSON(500, gin.H{"error": err.Error()})
		}
	}

	c.JSON(200, gin.H{
		"message": "handle ok.",
	})
}
