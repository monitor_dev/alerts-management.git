package controller

import (
	"encoding/json"
	"net/http"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/user_bot"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

// AlertToFeishuCustombot Alertmanger转发到飞书的自定义机器人
// @Summary  Alertmanger转发到飞书的自定义机器人
// @Description 支持转发到飞书群的自定义机器人,使用富文本信息
// @Tags Alertmanager转发用
// @Accept application/json
// @Produce application/json
// @Param feishu query string false "对应飞书配置的key"
// @Success 200 {object} _ResponsePostList
// @Router /alert/v1/alertmanager [post]
func CommonAlertToFeishuCustombot(c *gin.Context) {
	var alertJson dropbox.CommonAlert

	if err := c.ShouldBindBodyWith(&alertJson, binding.JSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	bt, _ := json.Marshal(alertJson)
	logger.Info(string(bt))
	// 获取告警组
	groups := alertJson.GetFlybookGroups()
	err := user_bot.FeishuSendCommMsgV2(&alertJson, groups)
	if err != nil {
		logger.Error("send msg card errors,by: ", err)
	}
	c.JSON(200, gin.H{
		"message": "handle ok.", "error": err,
	})
}
