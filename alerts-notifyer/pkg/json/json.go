package json

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/json-iterator/go/extra"
)

// 该配置已实现key排序
var json = jsoniter.ConfigCompatibleWithStandardLibrary

func init() {
	// RegisterFuzzyDecoders decode input from PHP with tolerance.
	//  It will handle string/number auto conversation, and treat empty [] as empty struct.
	extra.RegisterFuzzyDecoders()
}

func ToJson(obj interface{}) (string, error) {
	bs, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	return string(bs), nil
}

func ToJsonByte(obj interface{}) ([]byte, error) {
	bs, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return bs, nil
}

func ToJsonIgnoreError(obj interface{}) string {
	jsonStr, _ := ToJson(obj)
	return jsonStr
}

func FromJson(jsonStr string, obj interface{}) error {
	return json.Unmarshal([]byte(jsonStr), obj)
}

func FromJsonIgnoreError(jsonStr string, obj interface{}) {
	_ = FromJson(jsonStr, obj)
}
