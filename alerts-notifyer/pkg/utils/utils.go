package utils

import (
	"github.com/prometheus/alertmanager/template"
)

func Get(kv template.KV, key string) (value string) {
	value, ok := kv[key]
	if !ok {
		value = ""
	}
	return value
}

//指定多种分隔符
func Split(r rune) bool {
	return r == ',' || r == ';'
}

func InSlice(item string, slice []string) (bool, error) {
	/*switch itemType := item.(type)
	  case string:
	*/
	inFlag := false
	if slice == nil {
		return inFlag, nil
	}
	//if _, ok := item.(string); ok {
	for _, s := range slice {
		if s == item {
			inFlag = true
			break
		}
	}
	//} else {
	//	return inFlag, errors.New("bad type")
	//}
	return inFlag, nil
}
