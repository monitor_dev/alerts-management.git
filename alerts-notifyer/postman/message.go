package postman

type post_msg struct {
	labels   *map[string]string
	msg_info *msg_info
}

type msg_info struct {
	title     string
	content   string
	signature string
}
