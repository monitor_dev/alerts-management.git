package user_bot

import (
	//"errors"

	"errors"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
)

//type OpsApiData opsApi.AlertPostCommApi

type FeishuMsgPostBatchSend struct {
	Users   []string          `json:"user_ids"`
	MsgType string            `json:"msg_type"`
	Content map[string]string `json:"content"`
}

func OpsApiDataToFeishuBatchTemp(ops dropbox.AlertPostCommApi, users []string) *FeishuMsgPostBatchSend {
	return &FeishuMsgPostBatchSend{
		Users:   users,
		MsgType: "text",
		Content: map[string]string{
			"text": ops.Msg,
		},
	}
}

func (c *FeishuCert) FeishuMsgBatchSendWithPost(ops dropbox.AlertPostCommApi, users []string) error {
	authHeader, err := c.TokenHeader()
	if err != nil {
		return err
	}
	alarmJson, err := json.ToJson(OpsApiDataToFeishuBatchTemp(ops, users))
	if nil != err {
		logger.Error("decode err", err)
		return errors.New("decode err")
	}
	resp, err := http.PostRequest(FeishuMsgBatchSend, alarmJson, *authHeader, *headerJson)
	logger.Info("FeishuMsgBatchSendWithPost response : ", resp)
	if err != nil {
		logger.Error("FeishuMsgBatchSendWithPost failed by: ", err)
		return err
	}
	return nil
}

/*
res, err := customBot.CommSendPostMsg(url)
		logger.Infof("{\"信息内容\":\"%s\",\"请求返回是\":\"%s\"}", *content, res)
		//alert.ReportCC() 暂停统计
		if err != nil {
			logger.Error("send msg to group failed by ", err, " group is ", group)
			errors = append(errors, err)

			continue
			//异常组发送失败不退出，继续执行
			//return res, err
		}
*/
type MsgCardRequest struct {
	ChatId  string                 `json:"chat_id"`
	Card    map[string]interface{} `json:"card"`
	MsgType string                 `json:"msg_type"` //interactive"
}

type MsgCardResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func (c *FeishuCert) FeishuMsgCardBatchSend(content *string, chatids []string) error {
	Card := map[string]interface{}{}
	err := json.FromJson(*content, &Card)
	if err != nil {
		return err
	}
	authHeader, err := c.TokenHeader()
	if err != nil {
		return err
	}
	var errs []error
	for _, chatid := range chatids {
		alarmJson, err := json.ToJson(MsgCardRequest{ChatId: chatid, MsgType: "interactive", Card: Card})
		if nil != err {
			logger.Error("MsgCardRequest to json err:", err)
			errs = append(errs, err)
		}
		resp, err := http.PostRequest(FeishuMsgSend, alarmJson, *authHeader, *headerJson)
		if nil != err {
			logger.Error("MsgCardRequest post err:", err)
			errs = append(errs, err)
		}
		logger.Debug("返回body是：%v", resp)
		rsp := MsgCardResponse{}
		err = json.FromJson(resp, &rsp)
		if nil != err {
			logger.Error("unmarshal response  err:", err)
			errs = append(errs, err)
		}
		if rsp.Code != 0 {
			logger.Error("response code err:", rsp.Msg)
			errs = append(errs, errors.New(rsp.Msg))
		}

	}
	if len(errs) != 0 {
		return errors.New("many errors to send flybook groups")
	}
	return nil
}
