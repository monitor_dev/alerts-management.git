package user_bot

import "gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"

const (
	// 自建应用如下，应用商店应用去掉internal/
	FeishuTenantAccessToken = "https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal/"
	FeishuBatchGetId        = "https://open.feishu.cn/open-apis/user/v1/batch_get_id"
	FeishuMsgSend           = "https://open.feishu.cn/open-apis/message/v4/send/"
	FeishuMsgBatchSend      = "https://open.feishu.cn/open-apis/message/v4/batch_send/"
)

var headerJson = &http.HeaderOption{
	Name:  "Content-Type",
	Value: "application/json",
}
