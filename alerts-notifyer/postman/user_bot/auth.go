package user_bot

import (
	"bytes"
	"fmt"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/utils"
	"github.com/spf13/viper"
)

type FeishuCert struct {
	ID     string `json:"app_id"`
	Secret string `json:"app_secret"`
}

type FeishuTokenRespon struct {
	Code   uint   `json:"code"`
	Expire int    `json:"expire"`
	Msg    string `json:"msg"`
	Token  string `json:"tenant_access_token"`
}

func (c *FeishuCert) GetToken() (string, error) {

	jsonData, err := json.ToJson(c)
	if err != nil {
		return "", err
	}
	logger.Debug("user info to get token: " + jsonData)
	// 不添加头信息会返回参数错误
	tokenResp, err := http.PostRequest(FeishuTenantAccessToken, jsonData)
	if err != nil {
		return "", err
	}
	jsonResp := &FeishuTokenRespon{}
	logger.Debug("token info to response: " + tokenResp)
	err = json.FromJson(tokenResp, jsonResp)
	if err != nil {
		logger.Error(err)
		return "", err
	}
	logger.Debug("token is ", jsonResp.Token)
	return jsonResp.Token, nil
}

func feishuConvertToQueryParams(params map[string][]string) string {
	var buffer bytes.Buffer
	buffer.WriteString("?")
	emails, found := params["emails"]
	if found {
		for _, value := range emails {
			buffer.WriteString(fmt.Sprintf("%s=%v&", "emails", value))
		}
	}
	mobiles, found := params["mobiles"]
	if found {
		for _, value := range mobiles {
			buffer.WriteString(fmt.Sprintf("%s=%v&", "mobiles", value))
		}
	}
	buffer.Truncate(buffer.Len() - 1)
	return buffer.String()
}

var feishuBot FeishuCert

func InitFeishuBot() {
	id := viper.GetString("feishu.bot.id")
	secret := viper.GetString("feishu.bot.secret")
	if id == "" || secret == "" {
		panic("id or secret not founed in config file")
	}
	feishuBot = FeishuCert{
		ID:     viper.GetString("feishu.bot.id"),
		Secret: viper.GetString("feishu.bot.secret"),
	}

}

/*
{
    "code": 0,
    "msg": "success",
    "data": {
        "email_users": {
            "lisi@z.com": [
                {
                    "open_id": "ou_979112345678741d29069abcdef089d4",
                    "user_id": "a7eb3abe"
                }
            ]
        },
        "emails_not_exist": [
            "wangwu@z.com"
        ],
        "mobile_users": {
            "13812345678": [
                {
                    "open_id": "ou_46a087654321a1dc920ffab8fedc823f",
                    "user_id": "18fg19d4"
                }
            ]
        },
        "mobiles_not_exist": [
            "13912345678",
            "+12126668888"
        ]
    }
}
*/

type FeishuBatchGetIdResponse struct {
	Code int                 `json:"code"`
	Msg  string              `json:"msg"`
	Data FeishuUserBatchData `json:"data"`
}

type FeishuUserBatchData struct {
	Emails         map[string][]*FeishuUserData `json:"email_users"`
	EmailNotExist  []string                     `json:"emails_not_exist"`
	Mobiles        map[string][]*FeishuUserData `json:"mobile_users"`
	MobileNotExist []string                     `json:"mobiles_not_exist"`
}

type FeishuUserData struct {
	OpenId string `json:"open_id"`
	UserId string `json:"user_id"`
}

func (c *FeishuCert) TokenHeader() (*http.HeaderOption, error) {
	token, err := c.GetToken()
	if err != nil {
		logger.Error("get token by id and secret failed by :", err)
		return nil, err
	}
	logger.Info("token is  :", token)
	authHeader := http.BuildTokenHeaderOptions(token)
	return &authHeader, nil
}

func (c *FeishuCert) GetUserId(params map[string][]string) []string {
	authHeader, err := c.TokenHeader()
	if err != nil {
		return []string{}
	}
	logger.Debug("auth header:", authHeader)
	url := FeishuBatchGetId + feishuConvertToQueryParams(params)
	logger.Debug(url)
	jsonStr, err := http.GetRequest(url, *authHeader)
	if err != nil {
		logger.Error("call feishu get batch userid api failed by", err)
		// 获取失败则直接退出
		return []string{}
	}
	logger.Debug("call feishu get batch userid api,jsonstr is ", jsonStr)
	resp := &FeishuBatchGetIdResponse{}
	// FromJson跳过错误会导致空指针
	err = json.FromJson(jsonStr, resp)
	if err != nil {
		logger.Error("json 2 struct failed by: " + err.Error())
		return []string{}
	}
	logger.Debug("call feishu get batch userid api,response is ", resp)
	//users := make([]string, len(resp.Data.Emails)+len(resp.Data.Mobiles))
	users := []string{}
	for _, emailInfo := range resp.Data.Emails {
		for _, emailUser := range emailInfo {
			inFlag, err := utils.InSlice(emailUser.UserId, users)
			if err == nil && !inFlag {
				users = append(users, emailUser.UserId)
			}
		}
	}
	for _, mobileInfo := range resp.Data.Mobiles {
		for _, mobileUser := range mobileInfo {
			inFlag, err := utils.InSlice(mobileUser.UserId, users)
			if err == nil && !inFlag {
				users = append(users, mobileUser.UserId)
			}
		}
	}
	logger.Info("users ", users)
	return users
}
