package user_bot

import (
	"errors"
	"strings"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/utils"
	"github.com/spf13/viper"
)

func FeishuSendCommMsg(alert *dropbox.AlertPostCommApi) error {
	// 获取联系人信息
	emails := strings.FieldsFunc(utils.Get(alert.Labels, "flybook_emails"), utils.Split)
	mobiles := strings.FieldsFunc(utils.Get(alert.Labels, "flybook_mobiles"), utils.Split)
	if len(mobiles) == 0 && len(emails) == 0 {
		logger.Info("mobile and email  is all  nil ,skip")
		return nil
	}
	contactMap := map[string][]string{"emails": emails, "mobiles": mobiles}
	contactList := feishuBot.GetUserId(contactMap)
	if len(contactList) == 0 {
		logger.Error("use mobile and email to get userid is  nil ,mobiles:", mobiles, " emails", emails)
		return errors.New("use mobile and email to get userid is  nil ,sent msg failed")
	}
	err := feishuBot.FeishuMsgBatchSendWithPost(*alert, contactList)
	if err != nil {
		logger.Error("feishuBot.FeishuMsgBatchSendWithPost failed by: ", err.Error())
		return err
	}
	return nil
}

func FeishuSendCommMsgV2(alert *dropbox.CommonAlert, groups []interface{}) error {
	content := dropbox.CommAlertToFeishuPostTemplateV1(alert)
	var ChatIds []string
	for _, group := range groups {
		chatID := viper.GetString(strings.Join([]string{"feishu.group", group.(string), "chatid"}, "."))
		if chatID != "" {
			ChatIds = append(ChatIds, chatID)
		}
	}
	if len(ChatIds) == 0 {
		logger.Error("len(ChatIds) == 0")
	}
	logger.Info("chaids : %v", ChatIds)
	return feishuBot.FeishuMsgCardBatchSend(content, ChatIds)
}
