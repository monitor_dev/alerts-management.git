package custom_bot

import (
	"fmt"
	"strings"
	"time"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"github.com/spf13/viper"
)

/*
{
    "msg_type": "post",
    "content": {
        "post": {
            "zh_cn": {
                "title": "项目更新通知",
                "content": [
                    [
                        {
                            "tag": "text",
                            "text": "项目有更新: "
                        }
                    ]
                ]
            }
        }
    }
}
*/
type FeishuCustomBotCommV1 struct {
	Timestamp string `json:"timestamp,omitempty"`
	Sign      string `json:"sign,omitempty"`
	MsgType   string `json:"msg_type"`
	Content   string `json:"content"`
}

/*
// 飞书自定义机器人的签名
func FeishuCustomBotGenSign(secret string, timestamp int64) (string, error) {
	//timestamp + key 做sha256, 再进行base64 encode
	stringToSign := fmt.Sprintf("%v", timestamp) + "\n" + secret

	var data []byte
	h := hmac.New(sha256.New, []byte(stringToSign))
	_, err := h.Write(data)
	if err != nil {
		return "", err
	}

	signature := base64.StdEncoding.EncodeToString(h.Sum(nil))
	return signature, nil
}
*/
func (feishu *FeishuCustomBotCommV1) FeishuCustomBotSignature(secret string) error {
	timestamp := time.Now().Unix()
	signature, err := FeishuCustomBotGenSign(secret, timestamp)
	if err != nil {
		return err
	}
	feishu.Sign = signature
	feishu.Timestamp = fmt.Sprintf("%v", timestamp)
	return nil
}

func (feishu *FeishuCustomBotCommV1) CommSendPostMsg(url string) (string, error) {
	//   -H 'Content-Type: application/json' \
	headerJson := &http.HeaderOption{
		Name:  "Content-Type",
		Value: "application/json",
	}
	feishuJson, err := json.ToJson(feishu)
	if nil != err {
		logger.Fatal("decode err")
	}

	return http.PostRequest(url, feishuJson, *headerJson)
}

func FeishuSendCommMsgV1(alert *dropbox.CommonAlert, groups []interface{}) []error {
	content := dropbox.CommAlertToFeishuPostTemplateV1(alert)
	var errors []error
	for _, group := range groups {
		customBot := &FeishuCustomBotCommV1{
			MsgType: "post",
			Content: *content,
		}
		url := viper.GetString(strings.Join([]string{"feishu.custombot", group.(string), "url"}, "."))
		secret := viper.GetString(strings.Join([]string{"feishu.custombot", group.(string), "secret"}, "."))
		if secret != "" {
			customBot.FeishuCustomBotSignature(secret)
		}
		res, err := customBot.CommSendPostMsg(url)
		logger.Infof("{\"信息内容\":\"%s\",\"请求返回是\":\"%s\"}", *content, res)
		//alert.ReportCC() 暂停统计
		if err != nil {
			logger.Error("send msg to group failed by ", err, " group is ", group)
			errors = append(errors, err)

			continue
			//异常组发送失败不退出，继续执行
			//return res, err
		}
	}
	return errors
}
