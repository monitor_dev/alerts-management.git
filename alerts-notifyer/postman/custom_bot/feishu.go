package custom_bot

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/dropbox"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/utils"
	"github.com/spf13/viper"
)

/*
{
    "msg_type": "post",
    "content": {
        "post": {
            "zh_cn": {
                "title": "项目更新通知",
                "content": [
                    [
                        {
                            "tag": "text",
                            "text": "项目有更新: "
                        }
                    ]
                ]
            }
        }
    }
}
*/
type FeishuCustomBotComm struct {
	Timestamp string                  `json:"timestamp,omitempty"`
	Sign      string                  `json:"sign,omitempty"`
	MsgType   string                  `json:"msg_type"`
	Content   *FeishuCustomBotPostMsg `json:"content"`
}

type FeishuCustomBotPostMsg struct {
	Post map[string]*FeishuCustomBotPostMsgContent `json:"post"`
}

type FeishuCustomBotPostMsgContent struct {
	Title   string                            `json:"title"`
	Content []*FeishuCustomBotPostMsgTemplate `json:"content"`
}

type FeishuCustomBotPostMsgTemplate []*FeishuCustomBotPostMsgText

type FeishuCustomBotPostMsgText struct {
	Tag  string `json:"tag"`
	Text string `json:"text"`
}

func CommAlertToFeishuPostTemplate(alertComm *dropbox.AlertPostCommApi) *FeishuCustomBotComm {

	text := &FeishuCustomBotPostMsgText{
		Tag:  "text",
		Text: alertComm.Msg,
	}
	temps := &FeishuCustomBotPostMsgTemplate{text}
	content := &FeishuCustomBotPostMsgContent{
		Title:   alertComm.Title,
		Content: []*FeishuCustomBotPostMsgTemplate{temps},
	}
	post := &FeishuCustomBotPostMsg{
		Post: make(map[string]*FeishuCustomBotPostMsgContent),
	}
	post.Post["zh_cn"] = content
	res := &FeishuCustomBotComm{
		MsgType: "post",
		Content: post,
	}

	return res
}

// 飞书自定义机器人的签名
func FeishuCustomBotGenSign(secret string, timestamp int64) (string, error) {
	//timestamp + key 做sha256, 再进行base64 encode
	stringToSign := fmt.Sprintf("%v", timestamp) + "\n" + secret

	var data []byte
	h := hmac.New(sha256.New, []byte(stringToSign))
	_, err := h.Write(data)
	if err != nil {
		return "", err
	}

	signature := base64.StdEncoding.EncodeToString(h.Sum(nil))
	return signature, nil
}

func (feishu *FeishuCustomBotComm) FeishuCustomBotSignature(secret string) error {
	timestamp := time.Now().Unix()
	signature, err := FeishuCustomBotGenSign(secret, timestamp)
	if err != nil {
		return err
	}
	feishu.Sign = signature
	feishu.Timestamp = fmt.Sprintf("%v", timestamp)
	return nil
}

func (feishu *FeishuCustomBotComm) CommSendPostMsg(url string) (string, error) {
	//   -H 'Content-Type: application/json' \
	headerJson := &http.HeaderOption{
		Name:  "Content-Type",
		Value: "application/json",
	}
	feishuJson, err := json.ToJson(feishu)
	if nil != err {
		logger.Fatal("decode err")
	}

	return http.PostRequest(url, feishuJson, *headerJson)
}

func FeishuSendCommMsg(alert *dropbox.AlertPostCommApi, feishuTag string) error {
	// 获取告警组
	groups := strings.FieldsFunc(utils.Get(alert.Labels, "flybook_groups"), utils.Split)
	if len(groups) == 0 && feishuTag == "" {
		logger.Info("flybook_groups and feishuTag  is  all nil ,sent msg failed")
		return nil
	} else if len(groups) == 0 && len(feishuTag) != 0 {
		groups = append(groups, feishuTag)
	}
	for _, group := range groups {
		commAlert := CommAlertToFeishuPostTemplate(alert)
		url := viper.GetString(strings.Join([]string{"feishu.custombot", group, "url"}, "."))
		secret := viper.GetString(strings.Join([]string{"feishu.custombot", group, "secret"}, "."))
		if secret != "" {
			commAlert.FeishuCustomBotSignature(secret)
		}
		res, err := commAlert.CommSendPostMsg(url)
		logger.Infof("{\"信息标题\":\"%s\",\"信息内容\":\"%s\",\"标签\":\"%v\",\"请求返回是\":\"%s\"}", alert.Title, alert.Msg, alert.Labels, res)
		alert.ReportCC()
		if err != nil {
			logger.Error("send msg to group failed by ", err, " group is ", group)
			continue
			//异常组发送失败不退出，继续执行
			//return res, err
		}
	}
	return nil
}
