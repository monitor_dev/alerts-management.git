package feishu

import (
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/feishu/core"
)

//定义参照: https://open.feishu.cn/open-apis/message/v4/send/
type FeishuMsg struct {
	OpenId      string `json:"open_id,omitempty"`
	UserId      string `json:"user_id,omitempty"`
	Email       string `json:"email,omitempty"`
	ChatId      string `json:"chat_id,omitempty"`
	MsgType     string `json:"msg_type"`
	RootId      string `json:"root_id,omitempty"`
	UpdateMulti bool   `json:"update_multi"`

	Card    *Card       `json:"card,omitempty"`
	Content *MsgContent `json:"content,omitempty"`
}

type FeishuBatchMsg struct {
	DepartmentIds []string `json:"department_ids"`
	OpenIds       []string `json:"open_ids"`
	UserIds       []string `json:"user_ids"`
	MsgType       string   `json:"msg_type"`

	Card    *Card       `json:"card,omitempty"`
	Content *MsgContent `json:"content,omitempty"`
}

type MsgContent struct {
	Text     string   `json:"text"`
	ImageKey string   `json:"image_key"`
	Post     *MsgPost `json:"post,omitempty"`
}

type MsgPost struct {
	ZhCn *MsgPostValue `json:"zh_cn,omitempty"`
	EnUs *MsgPostValue `json:"en_us,omitempty"`
	JaJp *MsgPostValue `json:"ja_jp,omitempty"`
}

type MsgPostValue struct {
	Title   string      `json:"title"`
	Content interface{} `json:"content"`
}

//机器人消息Card字段数据格式定义
type Card struct {
	Config       *CardConfig     `json:"config,omitempty"`
	CardLink     *CardElementUrl `json:"card_link,omitempty"`
	Header       *CardHeader     `json:"header,omitempty"`
	Elements     []interface{}   `json:"elements"`
	I18nElements *I18nElement    `json:"i18n_elements,omitempty"`
}

type CardConfig struct {
	WideScreenMode bool `json:"wide_screen_mode"`
}

type CardHeader struct {
	Title *CardHeaderTitle `json:"title,omitempty"`
}

type CardHeaderTitle struct {
	Tag     string    `json:"tag"`
	Content string    `json:"content"`
	Lines   int       `json:"lines,omitempty"`
	I18n    *CardI18n `json:"i18n,omitempty"`
}

type CardI18n struct {
	ZhCn string `json:"zh_cn"`
	EnUs string `json:"en_us"`
	JaJp string `json:"ja_jp"`
}

type I18nElement struct {
	ZhCn []interface{} `json:"zh_cn"`
	EnUs []interface{} `json:"en_us"`
	JaJp []interface{} `json:"ja_jp"`
}

type CardElementUrl struct {
	Url        string `json:"url"`
	AndroidUrl string `json:"android_url"`
	IosUrl     string `json:"ios_url"`
	PcUrl      string `json:"pc_url"`
}

//发送消息卡片 https://open.feishu.cn/document/ukTMukTMukTM/uYTNwUjL2UDM14iN1ATN
func (t Tenant) SendMessage(msg FeishuMsg) (*core.MsgResp, error) {
	reqBody := json.ToJsonIgnoreError(msg)
	respBody, err := http.Post(core.ApiRobotSendMessage, nil, reqBody, http.BuildTokenHeaderOptions(t.TenantAccessToken))
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	respData := &core.MsgResp{}
	json.FromJsonIgnoreError(respBody, respData)
	return respData, nil
}

//发送消息卡片 https://open.feishu.cn/document/ukTMukTMukTM/uYTNwUjL2UDM14iN1ATN
func (t Tenant) SendMessageBatch(msg FeishuBatchMsg) (*core.MsgResp, error) {
	reqBody := json.ToJsonIgnoreError(msg)
	respBody, err := http.Post(core.ApiRobotSendBatchMessage, nil, reqBody, http.BuildTokenHeaderOptions(t.TenantAccessToken))
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	respData := &core.MsgResp{}
	json.FromJsonIgnoreError(respBody, respData)
	return respData, nil
}
