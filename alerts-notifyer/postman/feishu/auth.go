package feishu

import (
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/feishu/core"
)

type Tenant struct {
	TenantAccessToken string
}

//获取 tenant_access_token（企业自建应用）https://open.feishu.cn/document/ukTMukTMukTM/uIjNz4iM2MjLyYzM
func GetTenantAccessTokenInternal(appId string, appSecret string) (*core.TenantAccessTokenResp, error) {
	reqBody := map[string]interface{}{
		"app_id":     appId,
		"app_secret": appSecret,
	}
	respBody, err := http.Post(core.ApiTenantAccessTokenInternal, nil, json.ToJsonIgnoreError(reqBody))
	if err != nil {
		return nil, err
	}
	resp := &core.TenantAccessTokenResp{}
	json.FromJsonIgnoreError(respBody, resp)
	return resp, nil
}
