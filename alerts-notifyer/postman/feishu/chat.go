package feishu

import (
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/feishu/core"
)

//获取用户所在的群列表 https://open.feishu.cn/document/ukTMukTMukTM/uQzMwUjL0MDM14CNzATN
func (t Tenant) GroupList(userAccessToken string, pageSize int, pageToken string) (*core.GroupListResp, error) {
	queryParams := map[string]interface{}{}
	if pageSize > 0 {
		queryParams["page_size"] = pageSize
	}
	if pageToken != "" {
		queryParams["page_token"] = pageToken
	}
	respBody, err := http.Get(core.ApiUserGroupLIst, queryParams, http.BuildTokenHeaderOptions(userAccessToken))
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	respData := &core.GroupListResp{}
	json.FromJsonIgnoreError(respBody, respData)
	return respData, nil
}
