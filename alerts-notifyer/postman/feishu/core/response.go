package core

type CommonResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

//
type TenantAccessTokenResp struct {
	CommonResp
	TenantAccessToken string `json:"tenant_access_token"`
	Expire            int64  `json:"expire"`
}

//
type MsgResp struct {
	CommonResp

	Data MsgRespData `json:"data"`
}

type MsgRespData struct {
	MessageId string `json:"message_id"`
}

//
type GroupListResp struct {
	CommonResp

	Data *UserGroupListData `json:"data"`
}

type UserGroupListData struct {
	HasMore   bool        `json:"has_more"`
	PageToken string      `json:"page_token"`
	Groups    []GroupData `json:"groups"`
}

type GroupData struct {
	Avatar      string `json:"avatar"`
	ChatId      string `json:"chat_id"`
	Description string `json:"description"`
	Name        string `json:"name"`
	OwnerOpenId string `json:"owner_open_id"`
	OwnerUserId string `json:"owner_user_id"`
}

//
type ChatMembersResp struct {
	CommonResp
	Data *ChatGroupData `json:"data"`
}

type ChatGroupData struct {
	ChatId  string       `json:"chat_id"`
	HasMore bool         `json:"has_more"`
	Members []MemberData `json:"members"`
}

type MemberData struct {
	OpenId string `json:"open_id"`
	UserId string `json:"user_id"`
	Name   string `json:"name"`
}

//
type SearchUserResp struct {
	CommonResp
	Data *SearchUserRespData `json:"data"`
}

type SearchUserRespData struct {
	HasMore   bool             `json:"has_more"`
	PageToken string           `json:"page_token"`
	Users     []SearchUserInfo `json:"users"`
}

type SearchUserInfo struct {
	Avatar        UserAvatar `json:"avatar"`
	DepartmentIds []string   `json:"department_ids"`
	Name          string     `json:"name"`
	UserInfo
}

type UserAvatar struct {
	Avatar72     string `json:"avatar_72"`
	Avatar240    string `json:"avatar_240"`
	Avatar640    string `json:"avatar_640"`
	AvatarOrigin string `json:"avatar_origin"`
}

//
type UserInfo struct {
	OpenId string `json:"open_id"`
	UserId string `json:"user_id"`
}

type BatchGetIdResp struct {
	CommonResp
	Data *BatchGetIdRespData `json:"data"`
}

type BatchGetIdRespData struct {
}
