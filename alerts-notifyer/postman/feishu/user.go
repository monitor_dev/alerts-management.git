package feishu

import (
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/http"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/json"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/pkg/logger"
	"gitee.com/monitor_dev/alerts-management/alerts-notifyer/postman/feishu/core"
)

type User struct {
	UserAccessToken string
}

//搜索用户 https://bytedance.feishu.cn/docs/doccnizryz7NKuUmVfkRJWeZGVc
func (u User) SearchUser(query string, pageSize int, pageToken string) (*core.SearchUserResp, error) {
	queryParams := map[string]interface{}{}
	if query != "" {
		queryParams["query"] = query
	}
	if pageSize > 0 {
		queryParams["page_size"] = pageSize
	}
	if pageToken != "" {
		queryParams["page_token"] = pageToken
	}
	respBody, err := http.Get(core.ApiSearchUser, queryParams, http.BuildTokenHeaderOptions(u.UserAccessToken))
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	respData := &core.SearchUserResp{}
	json.FromJsonIgnoreError(respBody, respData)
	return respData, nil
}

/*
//使用手机号或邮箱获取用户ID https://open.feishu.cn/document/ukTMukTMukTM/uUzMyUjL1MjM14SNzITN
func (t Tenant) UserBatchGetID(mobiles, emails []string) {
	queryParams := map[string]interface{}{}
	respBody, err := http.Get(core.ApiUserBatchGetID, queryParams, http.BuildTokenHeaderOptions(t.TenantAccessToken))
}
*/
