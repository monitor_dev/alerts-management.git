#!/usr/bin/env bash

prog=simple_alert
version=0.1.1

# 交叉编译
CGO_ENABLED=1
GOOS=linux
GOARCH=amd64 
go build -ldflags "\
-w -s \
-X main.version=$version \
-X main.gitCommit=`git rev-parse HEAD` \
-X main.buildTime=`date -u '+%Y-%m-%d_%H:%M:%S'` \
" -v -o ${prog} cmd/simple_alert/main.go

# 交叉编译
CGO_ENABLED=1
GOOS=linux
GOARCH=amd64
go build -ldflags "\
-X main.version=$version \
-X main.gitCommit=`git rev-parse HEAD` \
-X main.buildTime=`date -u '+%Y-%m-%d_%H:%M:%S'` \
" -v -o ${prog}_dwarf cmd/simple_alert/main.go
