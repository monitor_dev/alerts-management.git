module gitee.com/monitor_dev/alerts-management/alerts-processer

go 1.16

require (
	github.com/alecthomas/units v0.0.0-20210927113745-59d0afb8317a // indirect
	github.com/go-kit/log v0.1.0
	github.com/prometheus/alertmanager v0.23.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.30.0
	github.com/prometheus/exporter-toolkit v0.7.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
